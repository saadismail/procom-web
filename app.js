require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const passport = require('passport');
const bodyParser = require('body-parser');
const errorHandler = require('utils/error-handler');
const auth = require('utils/auth');
const config = require('config');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors()); 

// api routes
app.use('/api/users', require('routes/user.route'));
app.use('/api/competitions', require('routes/competition.route'));
app.use('/api/backers', require('routes/sponsor.route')); // /sponsors gets blocked by ad blocker hence its /backers
app.use('/api/institutes', require('routes/institute.route'));
app.use('/api/teams', passport.authenticate('jwt', {session: false}), require('routes/team.route'));
app.use('/api/ambassadors', require('routes/ambassador.route'));
app.use('/api/announcements', require('routes/announcement.route'));
app.use('/api/payment-methods', require('routes/payment_method.route'));
app.use('/api/payment', passport.authenticate('jwt', {session: false}), require('routes/payment.route'));
app.use('/api/feedback', passport.authenticate('jwt', {session: false}), require('routes/feedback.route'));

// admin api routes
app.use('/api/admin/*', passport.authenticate('jwt', {session: false}));
app.use('/api/admin/promos', auth.authenticate_admin, require('routes/promo.route'));
app.use('/api/admin/backers', auth.authenticate_admin, require('routes/sponsor.admin.route')); // /sponsors gets blocked by ad blocker hence its /backers
app.use('/api/admin/ambassadors', auth.authenticate_admin, require('routes/ambassador.admin.route'));
app.use('/api/admin/competitions', auth.authenticate_admin, require('routes/competition.admin.route'));
app.use('/api/admin/payments', auth.authenticate_admin, require('routes/payment.admin.route'));
app.use('/api/admin/teams', auth.authenticate_admin, require('routes/team.admin.route'));

// global error handler
app.use(errorHandler);

app.use(passport.initialize());
app.use(passport.session());
require('utils/passport')(passport);

// start server
const port = config.nodePort;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});