const express = require('express');
const router = express.Router();
const sponsorService = require('services/sponsor.service');

// routes
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', remove);

function create(req, res, next) {
    sponsorService.create(req.body)
        .then(sponsor => sponsor ? res.json(sponsor) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    sponsorService.update(req.params.id, req.body)
        .then(sponsor => sponsor ? res.json(sponsor) : res.sendStatus(404))
        .catch(err => next(err));
}

function remove(req, res, next) {
    sponsorService.remove(req.params.id)
        .then(sponsor => sponsor ? res.json(sponsor) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;