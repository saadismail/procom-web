const express = require('express');
const router = express.Router();
const paymentService = require('services/payment.service');

// routes
router.get('/', getAll);
router.post('/verify', verifyPayment);

function getAll(req, res, next) {
    paymentService.getAll()
        .then(payments => res.json(payments))
        .catch(err => next(err));
}

function verifyPayment(req, res, next) {
    paymentService.verifyPayment(req.body.id, req.user)
        .then(payments => res.json(payments))
        .catch(err => next(err));
}

module.exports = router;