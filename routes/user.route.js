const express = require('express');
const router = express.Router();
const passport = require('passport');
const userService = require('services/user.service');
const constants = require('utils/constants');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', passport.authenticate('jwt', {session: false}), getById);
router.put('/', passport.authenticate('jwt', {session: false}), update);

module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json({ success: true, user }) : res.status(400).json({ success: false, message: constants.USER_AUTHENTICATION_FAILED }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then((result) => res.json(result))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.user._id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.user._id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}