const express = require('express');
const router = express.Router();
const competitionService = require('services/competition.service');

// routes
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', remove);

function create(req, res, next) {
    competitionService.create(req.body)
        .then(competition => competition ? res.json(competition) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    competitionService.update(req.params.id, req.body)
        .then(competition => competition ? res.json(competition) : res.sendStatus(404))
        .catch(err => next(err));
}

function remove(req, res, next) {
    competitionService.remove(req.params.id)
        .then(competition => competition ? res.json(competition) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;