const express = require('express');
const router = express.Router();
const ambassadorService = require('services/ambassador.service');
const passport = require('passport');
const auth = require('utils/auth');

// routes
router.get('/', getAll);
// router.get('/:id', getById);
router.get('/stats', passport.authenticate('jwt', {session: false}), auth.authenticate_ambassador, getStatsById)

function getAll(req, res, next) {
    ambassadorService.getAll()
        .then(ambassadors => ambassadors ? res.json(ambassadors) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    ambassadorService.getById(req.params.id)
        .then(ambassador => ambassador ? res.json(ambassador) : res.sendStatus(404))
        .catch(err => next(err));
}

function getStatsById(req, res, next) {
    ambassadorService.getStatsById(req.user._id)
        .then(stats => stats ? res.json(stats) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;