
const express = require('express');
const router = express.Router();
const teamService = require('services/team.service');

// routes
router.get('/', getAll);

function getAll(req, res, next) {
    teamService.getAll()
        .then(teams => res.json(teams))
        .catch(err => next(err));
}

module.exports = router;