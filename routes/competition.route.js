const express = require('express');
const router = express.Router();
const competitionService = require('services/competition.service');

// routes
router.get('/', getAll);
router.get('/mobile', getAllForMobile); // Specialized route for fetching data in mobile
router.get('/category/:category', getByCategory);
router.get('/:slug', getBySlug);
router.get('/id/:id', getById);

function getAll(req, res, next) {
    competitionService.getAll()
        .then(competitions => competitions ? res.json(competitions) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByCategory(req, res, next) {
    competitionService.getByCategory(req.params.category)
        .then(competitions => competitions ? res.json(competitions) : res.sendStatus(404))
        .catch(err => next(err));
}

function getBySlug(req, res, next) {
    competitionService.getBySlug(req.params.slug)
        .then(competition => competition ? res.json(competition) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    competitionService.getById(req.params.id)
        .then(competition => competition ? res.json(competition) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllForMobile(req, res, next) {
    competitionService.getAllForMobile()
        .then(competitions => competitions ? res.json(competitions) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;