const express = require('express');
const router = express.Router();
const paymentService = require('services/payment.service');

// routes
router.post('/ambassador', createAmbassadorPayment);
router.post('/self', createSelfPayment);

function createAmbassadorPayment(req, res, next) {
    const payParam = {
        teamCode: req.body.teamCode,
        total_fee: req.body.total_fee,
        ambassador: req.user._id
    }

    paymentService.createAmbassadorPayment(payParam)
        .then(team => res.json(team))
        .catch(err => next(err));
}

function createSelfPayment(req, res, next) {
    const payParam = {
        teamCode: req.body.teamCode,
        total_fee: req.body.total_fee,
        trx_id: req.body.trx_id,
        submittor: req.user._id
    }

    paymentService.createSelfPayment(payParam)
        .then(team => res.json(team))
        .catch(err => next(err));
}

module.exports = router;