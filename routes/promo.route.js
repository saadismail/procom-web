const express = require('express');
const router = express.Router();
const promoService = require('services/promo.service');

// routes
router.get('/', getAll);
router.post('/', create);
router.post('/:id/expire', expire);
router.put('/:id', update);
router.delete('/:id', remove);

function getAll(req, res, next) {
    promoService.getAll()
        .then(promos => promos ? res.json(promos) : res.sendStatus(404))
        .catch(err => next(err));
}

function create(req, res, next) {
    promoService.create(req.body)
        .then(promo => promo ? res.json(promo) : res.sendStatus(404))
        .catch(err => next(err));
}

function expire(req, res, next) {
    promoService.expire(req.params.id)
        .then(promos => promos ? res.json(promos) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    promoService.update(req.params.id)
        .then(promo => promo ? res.json(promo) : res.sendStatus(404))
        .catch(err => next(err));
}

function remove(req, res, next) {
    promoService.remove(req.params.id)
        .then(promo => res.json(promo))
        .catch(err => next(err));
}

module.exports = router;