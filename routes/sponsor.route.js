const express = require('express');
const router = express.Router();
const sponsorService = require('services/sponsor.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);
router.get('/category/:category', getByCategory);

function getAll(req, res, next) {
    sponsorService.getAll()
        .then(sponsors => sponsors ? res.json(sponsors) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    sponsorService.getById(req.params.id)
        .then(sponsor => sponsor ? res.json(sponsor) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByCategory(req, res, next) {
    sponsorService.getByCategory(req.params.category)
        .then(sponsors => sponsors ? res.json(sponsors) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;