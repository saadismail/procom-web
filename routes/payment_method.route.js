const express = require('express');
const router = express.Router();
const paymentMethodService = require('services/payment_method.service');

// routes
router.get('/', getAll);

function getAll(req, res, next) {
    paymentMethodService.getAll()
        .then(paymentMethods => paymentMethods ? res.json(paymentMethods) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;