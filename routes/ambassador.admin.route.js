const express = require('express');
const router = express.Router();
const ambassadorService = require('services/ambassador.service');

// routes
router.post('/', create);
router.put('/:id', update);
router.delete('/:id', remove);

function create(req, res, next) {
    ambassadorService.create(req.body)
        .then(ambassador => ambassador ? res.json(ambassador) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    ambassadorService.update(req.params.id, req.body)
        .then(ambassador => ambassador ? res.json(ambassador) : res.sendStatus(404))
        .catch(err => next(err));
}

function remove(req, res, next) {
    ambassadorService.remove(req.params.id)
        .then(ambassador => ambassador ? res.json(ambassador) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;