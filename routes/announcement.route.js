const express = require('express');
const router = express.Router();
const announcementService = require('services/announcement.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);

function getAll(req, res, next) {
    announcementService.getAll()
        .then(announcements => announcements ? res.json(announcements) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    announcementService.getById(req.params.id)
        .then(announcement => announcement ? res.json(announcement) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;