const express = require('express');
const router = express.Router();
const instituteService = require('services/institute.service');

// routes
router.get('/', getAll);

function getAll(req, res, next) {
    instituteService.getAll()
        .then(institutes => institutes ? res.json(institutes) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;