const express = require('express');
const router = express.Router();
const feedbackService = require('services/feedback.service');

router.post('/', submit);

function submit(req, res, next) {
    req.body.user = req.user._id; 
    feedbackService.submit(req.body)
        .then(result => res.json(result))
        .catch(err => next(err));
}

module.exports = router;