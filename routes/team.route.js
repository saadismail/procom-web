const express = require('express');
const router = express.Router();
const teamService = require('services/team.service');

// routes
router.post('/', register);
router.get('/', getAll);
router.get('/code/:code', getByTeamCode);
router.get('/ambassador/:ambassador_id', getAllByAmbassadorId);

function register(req, res, next) {
    req.body.leader = req.user._id;
    teamService.create(req.body)
        .then(team => res.json(team))
        .catch(err => next(err));
}

function getByTeamCode(req, res, next) {
    teamService.getByTeamCode(req.params.code, req.user._id)
        .then(team => team ? res.json(team) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    //get's all teams of the user who initiated the request
    teamService.getAllByLeaderId(req.user._id)
        .then(teams => teams ? res.json(teams) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAllByAmbassadorId(req, res, next) {
    teamService.getAllByAmbassadorId(req.user._id)
        .then(teams => teams ? res.json(teams) : res.sendStatus(404))
        .catch(err => next(err));
}

module.exports = router;