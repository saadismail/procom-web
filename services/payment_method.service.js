const db = require('utils/db');
const PaymentMethod = db.PaymentMethod;

async function getAll() {
    return await PaymentMethod.find();
}

module.exports = {
    getAll
}