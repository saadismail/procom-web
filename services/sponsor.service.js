const db = require('utils/db');
const Sponsor = db.Sponsor;
const constants = require('utils/constants');

async function create(sponsorParam) {
    const sponsor = new Sponsor(sponsorParam);
    try {
        await sponsor.save();
    } catch {
        return({
            success: false,
            message: constants.SPONSOR_CREATION_FAILED
        })
    }

    return({
        success: true,
        sponsor: sponsor,
        message: constants.SPONSOR_CREATED_SUCCESSFULLY
    })
}

async function update(id, sponsorParam) {
    const sponsor = await Sponsor.findById(id);
    if(!sponsor) return({
        success: false,
        message: constants.SPONSOR_NOT_FOUND
    });

    Object.assign(sponsor, sponsorParam);
    try {
        await sponsor.save();
    } catch {
        return({
            success: false,
            message: constants.SPONSOR_UPDATE_FAILED
        });
    }

    return({
        success: true,
        message: constants.SPONSOR_UPDATED_SUCCESSFULLY
    })
}

async function remove(id) {
    const sponsor = await Sponsor.findByIdAndRemove(id);
    if(sponsor) return({
        success: true,
        message: constants.SPONSOR_REMOVED_SUCCESSFULLY
    });

    return({
        success: false,
        message: constants.SPONSOR_REMOVE_FAILED
    })
}

async function getAll() {
    return await Sponsor.find();
}

async function getByCategory(category) {
    return await Sponsor.find({ level: category });
}

async function getById(id) {
    return await Sponsor.findById(id);
}

module.exports = {
    getAll,
    getByCategory,
    getById,
    create,
    update,
    remove
}