const db = require('utils/db');
const constants = require('utils/constants');
const Promo = db.Promo;

async function getAll() {
    return await Promo.find();
}

async function create(promoParam) {
    const promo = new Promo(promoParam);
    try {
        await promo.save();
    } catch {
        return({
            success: false,
            message: constants.PROMO_CREATION_FAILED
        });
    }

    return({
        success: true,
        message: constants.PROMO_CREATION_SUCCESSFULL,
        promo: promo
    });
}

async function expire(id) {
    const promo = await Promo.findById(id);
    if(!promo) return({
        success: false,
        message: constants.PROMO_NOT_FOUND
    });
    
    promo.expiry_date = Date.now();
    try {
        await promo.save();
    } catch {
        return({
            success: false,
            message: constants.PROMO_EXPIRATION_FAILED
        })
    };

    return ({
        success: true,
        message: constants.PROMO_EXPIRATION_SUCCESSFULL,
        promo: promo
    })
}

async function update(id, promoParam) {
    const promo = await Promo.findById(id);
    if(!promo) return({
        success: false,
        message: constants.PROMO_NOT_FOUND
    });

    Object.assign(promo, promoParam);
    try {
        await promo.save();
    } catch {
        return({
            success: false,
            message: constants.PROMO_UPDATE_FAILED
        });
    }

    return({
        success: true,
        message: constants.PROMO_UPDATE_SUCCESSFULL
    });
}

async function remove(id) {
    const promo = await Promo.findByIdAndRemove(id);
    if(promo) return({
        success: true,
        message: constants.PROMO_REMOVED_SUCCESSFULLY
    });

    return({
        success: false,
        message: constants.PROMO_REMOVE_FAILED
    })
}

module.exports = {
    getAll,
    create,
    expire,
    update,
    remove
}