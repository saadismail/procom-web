const db = require('utils/db');
const Announcement = db.Announcement;

async function getAll() {
    return await Announcement.find();
}

async function getById(id) {
    return await Announcement.findById(id);
}

module.exports = {
    getAll,
    getById
}