const db = require('utils/db');
const Institute = db.Institute;

async function getAll() {
    return await Institute.find().sort({name: 1});
}

module.exports = {
    getAll
}