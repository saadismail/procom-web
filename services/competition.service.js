const db = require('utils/db');
const Competition = db.Competition;
const constants = require('utils/constants');

async function create(competitionParam) {
    const competition = new Competition(competitionParam);
    try {
        await competition.save();
    } catch(e) {
        console.log(e);
        return({
            success: false,
            message: constants.COMPETITION_CREATION_FAILED
        })
    }

    return({
        success: true,
        competition: competition,
        message: constants.COMPETITION_CREATED_SUCCESSFULLY
    })
}

async function update(id, competitionParam) {
    const competition = await Competition.findById(id);
    if(!competition) return({
        success: false,
        message: constants.COMPETITION_NOT_FOUND
    });

    Object.assign(competition, competitionParam);
    try {
        await competition.save();
    } catch(e) {
        return({
            success: false,
            message: constants.COMPETITION_UPDATE_FAILED
        });
    }

    return({
        success: true,
        message: constants.COMPETITION_UPDATED_SUCCESSFULLY
    })
}

async function remove(id) {
    const competition = await Competition.findByIdAndRemove(id);
    if(competition) return({
        success: true,
        message: constants.COMPETITION_REMOVED_SUCCESSFULLY
    });

    return({
        success: false,
        message: constants.COMPETITION_REMOVE_FAILED
    })
}

async function getAll() {
    return await Competition.find();
}

async function getAllForMobile() {
    return {
        "CS": await Competition.find({ category: "CS" }).select("_id slug name min_members max_members"),
        "EE": await Competition.find({ category: "EE" }).select("_id slug name min_members max_members"),
        "BBA": await Competition.find({ category: "BBA" }).select("_id slug name min_members max_members"),
        "GAM": await Competition.find({ category: "GAM" }).select("_id slug name min_members max_members"),
        "GEN": await Competition.find({ category: "GEN" }).select("_id slug name min_members max_members")
    };
}

async function getByCategory(category) {
    return await Competition.find({ category: category }).select("_id slug name photo description");
}

async function getById(id) {
    return await Competition.findById(id);
}

async function getBySlug(slug) {
    return await Competition.findOne( { slug: slug } );
}

module.exports = {
    create,
    update,
    remove,
    getAll,
    getByCategory,
    getById,
    getBySlug,
    getAllForMobile
}