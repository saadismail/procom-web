const db = require('utils/db');
const constants = require('utils/constants');
const Payment = db.Payment;
const User = db.User;
const Team = db.Team;
const Competition = db.Competition;
const mail = require('utils/mail');

function validateSelfPaymentParam(payParam) {
    return (payParam.teamCode && payParam.total_fee && payParam.trx_id && payParam.submittor);
}

function validateAmbassadorPaymentParam(payParam) {
    return (payParam.teamCode && payParam.total_fee && payParam.ambassador);
}

async function errorForExistingPayments(teamId) {
    const payment = await Payment.findOne({ team: teamId });

    // Check if there is already a payment document
    if (payment) {
        if (payment.is_verified) {
            return constants.PAYMENT_TEAM_ALREADY_PAID;
        } else {
            return constants.PAYMENT_ALREADY_PENDING;
        }
    }

    return null; // All good
}

function isAmountValid(sent_amount, desired_amount) {
    return (sent_amount == desired_amount);
}

async function errorForAmbassador(ambassador_id) {
    const ambassador = await User.findById(ambassador_id);

    if (!ambassador || !ambassador.is_ambassador) {
        return constants.PAYMENT_AMBASSADOR_NOT_EXIST;
    }

    return null; // All good
}

async function createPayment(paymentParam) {
    const payment = new Payment(paymentParam);

    try {
        await payment.save();
    } catch {
        return false;
    }

    return true;
}

async function createSelfPayment(payParam) {
    // payParam must has teamCode, total_fee and trx_id
    if (!validateSelfPaymentParam(payParam)) {
        return({ success: false, message: constants.SOMETHING_WENT_WRONG });
    }

    const team = await Team.findOne({ code: payParam.teamCode } );
    
    if (!team) {
        return ( { success: false, message: constants.PAYMENT_TEAM_NOT_EXIST } )
    }

    if (!isAmountValid(payParam.total_fee, team.total_fee)) {
        return({ success: false, message: constants.PAYMENT_AMOUNT_INVALID });
    }
    
    if ((error = await errorForExistingPayments(team._id)) != null) {
        return({ success: false, message: error });
    }

    let paymentParam = {
        team: team._id,
        total_fee: payParam.total_fee,
        trx_id: payParam.trx_id,
        submittor: payParam.submittor,
        is_verified: false
    };

    if (await createPayment(paymentParam)) {
        return({ success: true, message: constants.PAYMENT_SUBMITTED_SUCCESSFULLY })
    } else {
        return({ success: false, message: constants.PAYMENT_SUBMISSION_FAILED })
    }
}

// takes team, total_fee, ambassador
async function createAmbassadorPayment(payParam) {
    // payParam must has teamCode, total_fee, ambassador
    if (!validateAmbassadorPaymentParam(payParam)) {
        return({ success: false, message: constants.SOMETHING_WENT_WRONG });
    }

    if ((error = await errorForAmbassador(payParam.ambassador)) != null) {
        return ( { success: false, message: error } )
    }

    const team = await Team.findOne({ code: payParam.teamCode} );

    if (!team) {
        return ( { success: false, message: constants.PAYMENT_TEAM_NOT_EXIST } )
    }

    if (!isAmountValid(payParam.total_fee, team.total_fee)) {
        return({ success: false, message: constants.PAYMENT_AMOUNT_INVALID });
    }

    if ((error = await errorForExistingPayments(team._id)) != null) {
        return({ success: false, message: error });
    }
    
    let paymentParam = {
        team: team._id,
        total_fee: payParam.total_fee,
        ambassador: payParam.ambassador,
        is_verified: true
    };

    const competition = await Competition.findById(team.competition);

    if (await createPayment(paymentParam)) {
        let emailText = mail.updateEmail(constants.EMAIL_TEAM_PAYMENT_BODY_TEXT,
            competition.name, team.name, team.code, team.total_fee);

        let emailHTML = mail.updateEmail(constants.EMAIL_TEAM_PAYMENT_BODY_HTML,
            competition.name, team.name, team.code, team.total_fee);

        await mail.sendMail(team.members[0].email, constants.EMAIL_TEAM_PAYMENT_SUBJECT,
            emailText, emailHTML);
    } else {
        return({ success: false, message: constants.PAYMENT_SUBMISSION_FAILED })
    }

    return({ success: true, message: constants.PAYMENT_SUBMITTED_SUCCESSFULLY })
}

async function getAll() {
    return await Payment.find().populate('team').populate('ambassador').populate('submittor').populate('verifier');
}

async function verifyPayment(id, user) {
    const payment = await Payment.findById(id).populate('team').populate('submittor');
    if(!payment) return({
        success: false,
        message: constants.PAYMENT_DOESNT_EXIST
    });

    if(payment.is_verified) {
        return({
            success: false,
            message: constants.PAYMENT_ALREADY_VERIFIED
        });
    }

    payment.verifier = user._id;
    payment.verify_date = Date.now();
    payment.is_verified = true;
    
    try {
        payment.save();
    } catch(e) {
        return({
            success: false,
            message: constants.PAYMENT_VERIFICATION_FAILED
        });
    }

    const competition = await Competition.findById(payment.team.competition);

    let emailText = mail.updateEmail(constants.EMAIL_TEAM_PAYMENT_VERIFICATION_BODY_TEXT,
        competition.name, payment.team.name, payment.team.code, payment.team.total_fee);

    let emailHTML = mail.updateEmail(constants.EMAIL_TEAM_PAYMENT_VERIFICATION_BODY_HTML,
        competition.name, payment.team.name, payment.team.code, payment.team.total_fee);

    await mail.sendMail(payment.submittor.email, constants.EMAIL_TEAM_PAYMENT_VERIFICATION_SUBJECT,
        emailText, emailHTML);

    return({
        success: true,
        message: constants.PAYMENT_VERIFIED_SUCCESSFULYY,
        verify_date: payment.verify_date
    });
}

module.exports = {
    createSelfPayment,
    createAmbassadorPayment,
    getAll,
    verifyPayment
};