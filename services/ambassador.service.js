const db = require('utils/db');
const Ambassador = db.Ambassador;
const Payment = db.Payment;
const constants = require('utils/constants');

async function create(ambassadorParam) {
    const ambassador = new Ambassador(ambassadorParam);
    try {
        await ambassador.save();
    } catch(e) {
        return({
            success: false,
            message: constants.AMBASSADOR_CREATION_FAILED
        })
    }

    return({
        success: true,
        ambassador: ambassador,
        message: constants.AMBASSADOR_CREATED_SUCCESSFULLY
    })
}

async function update(id, ambassadorParam) {
    const ambassador = await Ambassador.findById(id);
    if(!ambassador) return({
        success: false,
        message: constants.AMBASSADOR_NOT_FOUND
    });

    Object.assign(ambassador, ambassadorParam);
    try {
        await ambassador.save();
    } catch(e) {
        return({
            success: false,
            message: constants.AMBASSADOR_UPDATE_FAILED
        });
    }

    return({
        success: true,
        message: constants.AMBASSADOR_UPDATED_SUCCESSFULLY
    })
}

async function remove(id) {
    const ambassador = await Ambassador.findByIdAndRemove(id);
    if(ambassador) return({
        success: true,
        message: constants.AMBASSADOR_REMOVED_SUCCESSFULLY
    });

    return({
        success: false,
        message: constants.AMBASSADOR_REMOVE_FAILED
    })
}

async function getAll() {
    return await Ambassador.aggregate(
        [
            {
                $group: {
                    _id: "$institute",
                    ambassadors: {
                        $addToSet: {
                            "_id": "$_id",
                            "name": "$name",
                            "email": "$email",
                            "phone_no": "$phone_no",
                            "department": "$department",
                            "photo": "$photo"
                        }
                    }
                }
            },
            {
                $lookup: {
                    from: 'institutes', localField: '_id', foreignField: '_id', as: 'institute'
                }
            },
            {
                "$project": {
                    "name": "$institute.name",
                    "ambassadors": "$ambassadors"
                }
            },
            {
                "$unwind": "$name"
            }
        ]
    )
}

async function getById(id) {
    return await Ambassador.findById(id);
}

async function getStatsById(id) {
    let res = await Payment.aggregate(
        [
            {
                $match: {
                    ambassador: id
                }
            },
            {
                $group: {
                    _id: null,
                    team_count: {
                        $sum: 1
                    },
                    amount_paid: {
                        "$sum": "$total_fee"
                    }
                }
            }
        ]
    )
    return (res.length == 0) ? { team_count: 0, amount_paid: 0 } : res[0];
}

module.exports = {
    create,
    update,
    remove,
    getAll,
    getById,
    getStatsById
}