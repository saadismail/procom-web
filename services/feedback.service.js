const db = require('utils/db');
const constants = require('utils/constants');
const Feedback = db.Feedback;

async function submit(feedbackParam) {
    const feedback = new Feedback(feedbackParam);

    try {
        await feedback.save();
    } catch (err) {
        return ({ success: false, message: constants.FEEDBACK_SUBMISSION_FAILED });
    }

    return ({ success: true, message: constants.FEEDBACK_SUBMITTED_SUCCESSFULLY });
}

module.exports = {
    submit
}