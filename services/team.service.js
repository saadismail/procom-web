const db = require('utils/db');
const constants = require('utils/constants');
const Team = db.Team;
const Competition = db.Competition;
const Payment = db.Payment;
const Promo = db.Promo;
const mongoose = require('mongoose');
const mail = require('utils/mail');

function generateCode() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (let i = 0; i < 7; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

async function getByTeamCode(team_code, leader_id) {
    return await Team.find({
        code: team_code,
        leader: leader_id
    });
}

async function getAllByLeaderId(leader_id) {
    return await Team.aggregate([{
            $match: {
                leader: mongoose.Types.ObjectId(leader_id)
            }
        },
        {
            $lookup: {
                from: 'payments',
                localField: '_id',
                foreignField: 'team',
                as: 'payment'
            }
        },
        {
            $unwind: {
                path: "$payment",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'competitions',
                localField: 'competition',
                foreignField: '_id',
                as: 'competition'
            }
        },
        {
            $unwind: {
                path: "$competition"
            }
        },
        {
            $project: {
                competition: {
                    "rules": 0,
                    "category": 0,
                    "min_members": 0,
                    "max_members": 0,
                    "per_member_fee": 0,
                    "photo": 0,
                    "description": 0,
                    "judging_criteria": 0,
                    // "winning_criteria": 0,
                    "slug": 0,
                    "__v": 0
                }
            }
        }
    ])
}

async function getAllByAmbassadorId(ambassador_id) {
    return await Payment.find({
        ambassador: ambassador_id
    }).populate({
        path: 'team',
        populate: {
            path: 'leader',
            select: '-hash -__v -createdDate -is_ambassador -is_admin -_id'
        },
        select: '-members'
    });
}

async function create(teamParam) {
    let isPromoValid = false;

    // check if promo exists and valid
    if (teamParam.promo_code) {
        const promo = await Promo.findOne({
            code: teamParam.promo_code
        });

        if (promo === null) return ({
            success: false,
            message: constants.TEAM_PROMO_NOT_FOUND
        });

        if (promo.expiry_date <= Date.now()) return ({
            success: false,
            message: constants.TEAM_PROMO_NOT_FOUND
        });

        isPromoValid = true;
    }

    // calculate total fees
    const competition = await Competition.findById(teamParam.competition);
    teamParam.total_fee = competition.per_member_fee * teamParam.members.length;

    if (isPromoValid) {
        teamParam.total_fee = teamParam.total_fee * 0.9;
    }

    if (teamParam.is_accomodation) {
        teamParam.total_fee = teamParam.total_fee + (constants.ACCOMODATION_CHARGES_PER_HEAD * teamParam.members.length);
    }

    // assign random code to team
    teamParam.code = generateCode();

    // keep re-assigning until unique code is generated
    const teams = await Team.find();
    while (teams.some(team => team.code == teamParam.code)) teamParam.code = generateCode();

    const team = new Team(teamParam);
    try {
        await team.save();
    } catch {
        return ({
            success: false,
            message: constants.TEAM_REGISTRATION_FAILED
        })
    }

    try {
        let emailText = mail.updateEmail(constants.EMAIL_TEAM_REGISTER_BODY_TEXT,
            competition.name, team.name, team.code, team.total_fee);

        let emailHTML = mail.updateEmail(constants.EMAIL_TEAM_REGISTER_BODY_HTML,
            competition.name, team.name, team.code, team.total_fee);

        await mail.sendMail(teamParam.members[0].email, constants.EMAIL_TEAM_REGISTER_SUBJECT,
            emailText, emailHTML);
    } catch {
        // team registrations emails are not critical
        // do nothing when email could not be sent
    }

    return ({
        success: true,
        message: constants.TEAM_REGISTERED_SUCCESSFULLY
    })
}

async function getAll() {
    return Team.aggregate([
        
        {
            $lookup: {
                from: 'competitions',
                localField: 'competition',
                foreignField: '_id',
                as: 'competition'
            }
        },
        {
            "$unwind": {
                path: "$competition",
                includeArrayIndex: "i"
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'leader',
                foreignField: '_id',
                as: 'leader'
            }
        },
        {
            $lookup: {
                from: 'institutes',
                localField: 'leader.institute',
                foreignField: '_id',
                as: 'institute'
            }
        },
        {
            "$unwind": {
                path: "$institute",
                includeArrayIndex: "i"
            }
        },
        {
            "$unwind": {
                path: "$members",
                includeArrayIndex: "i"
            }
        },
        {
            "$group": {
                "_id": "$_id",
                "team_name": {
                    $first: "$name"
                },
                "total_fee": {
                    $first: "$total_fee"
                },
                "team_code": {
                    $first: "$code"
                },
                "is_accomodation": {
                    $first: "$is_accomodation"
                },
                "competition": {
                    $first: "$competition.name"
                },
                "institute": {
                    $first: "$institute.name"
                },
                "promo_code": {
                    $first: "$promo_code"
                },
                "mname": {
                    "$push": {
                        "k": {
                            "$concat": [
                                "member",
                                {
                                    "$toLower": "$i"
                                },
                                "_name"
                            ]
                        },
                        "v": "$members.name"
                    }
                },
                "memail": {
                    "$push": {
                        "k": {
                            "$concat": [
                                "member",
                                {
                                    "$toLower": "$i"
                                },
                                "_email"
                            ]
                        },
                        "v": "$members.email"
                    }
                },
                "mphone_no": {
                    "$push": {
                        "k": {
                            "$concat": [
                                "member",
                                {
                                    "$toLower": "$i"
                                },
                                "_phone_no"
                            ]
                        },
                        "v": "$members.phone_no"
                    }
                },
                "mcnic": {
                    "$push": {
                        "k": {
                            "$concat": [
                                "member",
                                {
                                    "$toLower": "$i"
                                },
                                "_cnic"
                            ]
                        },
                        "v": "$members.cnic"
                    }
                }
            }
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$$ROOT",
                        {
                            "$arrayToObject": "$mname"
                        },
                        "$$ROOT",
                        {
                            "$arrayToObject": "$memail"
                        },
                        "$$ROOT",
                        {
                            "$arrayToObject": "$mphone_no"
                        },
                        "$$ROOT",
                        {
                            "$arrayToObject": "$mcnic"
                        }
                    ]
                }
            }
        },
        {
            "$project": {
                "_id": 0,
                "mname": 0,
                "memail": 0,
                "mphone_no": 0,
                "mcnic": 0
            }
        }
    ])
}

module.exports = {
    getAllByLeaderId,
    getByTeamCode,
    getAllByAmbassadorId,
    create,
    getAll
}