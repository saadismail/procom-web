const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('utils/db');
const constants = require('utils/constants');
const User = db.User;

async function authenticate({ email, password }) {
    const user = await User.findOne({ email });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign(user.toJSON(), config.passportSecret, { expiresIn: "3d" });
        return {
            ...userWithoutHash,
            "token": "JWT " + token
        };
    }
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function getByEmail(email) {
    return await User.findOne(email).select('-hash');
}

async function create(userParam) {
    // validate
    if (await User.findOne({ email: userParam.email })) {
        return ( { success: false, message: constants.USER_EMAIL_ALREADY_TAKEN } )
    }

    const { is_ambassador, is_admin, ...userParamWithoutFields } = userParam;
    const user = new User(userParamWithoutFields);

    // hash password
    if (userParamWithoutFields.password) {
        user.hash = bcrypt.hashSync(userParamWithoutFields.password, 10);
    }

    try {
        await user.save();
    } catch {
        return ( { success: false, message: constants.USER_REGISTRATION_FAILED } )
    }

    return ( { success: true, message: constants.USER_REGISTERED_SUCCESSFULLY } )
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw constants.USER_NOT_FOUND;
    if (user.email !== userParam.email && await User.findOne({ email: userParam.email })) {
        throw constants.USER_EMAIL_ALREADY_TAKEN;
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // make sure that these values are not modified in the database
    const { is_admin, is_ambassador, ...userParamWithoutFields } = userParam;

    // copy userParam properties to user
    Object.assign(user, userParamWithoutFields);

    await user.save();
}

module.exports = {
    authenticate,
    getById,
    getByEmail,
    create,
    update
};