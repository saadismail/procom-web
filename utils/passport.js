const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const userService = require('services/user.service');
const config = require('config');

async function verifyUser(jwt_payload, done) {
    const user = await userService.getById(jwt_payload._id);

    if (user) {
        done(null, user);
    } else {
        done(null, false);
    }
}

module.exports = async function(passport) {
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
    opts.secretOrKey = config.passportSecret;
    
    passport.use(new JwtStrategy(opts,  verifyUser))
}