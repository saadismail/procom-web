module.exports = {

    SOMETHING_WENT_WRONG: "Something went wrong.",
    EMAIL_COULD_NOT_BE_SENT: "Email could not be send.",

    // Error Handlers
    VALIDATION_ERROR: "ValidationError",
    UNAUTHORIZED_ERROR: "UnauthorizedError",
    INVALID_TOKEN: "Invalid Token",

    // User Route
    USER_REGISTERED_SUCCESSFULLY: "Registered successfully.",
    USER_REGISTRATION_FAILED: "Registration failed.",
    USER_EMAIL_ALREADY_TAKEN: "An account already exists with this email.",
    USER_NOT_FOUND: "User not found.",
    USER_AUTHENTICATION_FAILED: "Email or password is incorrect.",

    // Team Route
    TEAM_REGISTERED_SUCCESSFULLY: "Team registered successfully.",
    TEAM_REGISTRATION_FAILED: "Team registration failed.",
    TEAM_PROMO_NOT_FOUND: "This promo code doesn't exist.",

    // Promo Route
    PROMO_CREATION_FAILED: "Promo creation failed.",
    PROMO_CREATION_SUCCESSFULL: "Promo created successfully.",
    PROMO_EXPIRATION_FAILED: "Promo couldn't be expired.",
    PROMO_EXPIRATION_SUCCESSFULL: "Promo expired successfully.",
    PROMO_NOT_FOUND: "Promo not found.",
    PROMO_UPDATE_SUCCESSFULL: "Promo updated successfully.",
    PROMO_REMOVED_SUCCESSFULLY: "Promo removed succesfully",
    PROMO_REMOVE_FAILED: "Promo couldn't be removed.",
    
    // Payment/Payment Verification Route
    PAYMENT_TEAM_NOT_EXIST: "Team does not exist.",
    PAYMENT_ALREADY_PENDING: "There is already a pending request for this team",
    PAYMENT_TEAM_ALREADY_PAID: "Team has already paid.",
    PAYMENT_SUBMISSION_FAILED: "Payment submission failed.",
    PAYMENT_SUBMITTED_SUCCESSFULLY: "Payment submitted successfully.",
    PAYMENT_AMBASSADOR_NOT_EXIST: "Ambassador does not exist.",
    PAYMENT_AMOUNT_INVALID: "Amount is invalid.",
    PAYMENT_DOESNT_EXIST: "Payment doesn't exist.",
    PAYMENT_VERIFIED_SUCCESSFULLY: "Payment verified successfully.",
    PAYMENT_VERIFICATION_FAILED: "Payment verification failed.",
    PAYMENT_ALREADY_VERIFIED: "Payment already verified",

    // Sponsor Route
    SPONSOR_CREATION_FAILED: "Sponsor creation failed.",
    SPONSOR_CREATED_SUCCESSFULLY : "Sponsor created successfully.",
    SPONSOR_NOT_FOUND: "Sponsor doesn't exist",
    SPONSOR_UPDATE_FAILED: "Sponsor couldn't be updated.",
    SPONSOR_UPDATED_SUCCESSFULLY: "Sponsor updated successfully.",
    SPONSOR_REMOVED_SUCCESSFULLY: "Sponsor removed successfully.",
    SPONSOR_REMOVE_FAILED: "Sponsor couldn't be removed.",

    // Ambassador Route
    AMBASSADOR_CREATION_FAILED: "Ambassador creation failed.",
    AMBASSADOR_CREATED_SUCCESSFULLY : "Ambassador created successfully.",
    AMBASSADOR_NOT_FOUND: "Ambassador doesn't exist",
    AMBASSADOR_UPDATE_FAILED: "Ambassador couldn't be updated.",
    AMBASSADOR_UPDATED_SUCCESSFULLY: "Ambassador updated successfully.",
    AMBASSADOR_REMOVED_SUCCESSFULLY: "Ambassador removed successfully.",
    AMBASSADOR_REMOVE_FAILED: "Ambassador couldn't be removed.",

    // Competition Route
    COMPETITION_CREATION_FAILED: "Competition creation failed.",
    COMPETITION_CREATED_SUCCESSFULLY : "Competition created successfully.",
    COMPETITION_NOT_FOUND: "Competition doesn't exist",
    COMPETITION_UPDATE_FAILED: "Competition couldn't be updated.",
    COMPETITION_UPDATED_SUCCESSFULLY: "Competition updated successfully.",
    COMPETITION_REMOVED_SUCCESSFULLY: "Competition removed successfully.",
    COMPETITION_REMOVE_FAILED: "Competition couldn't be removed.",

    // Feedback Route
    FEEDBACK_SUBMITTED_SUCCESSFULLY: "Feedback has been submitted successfully.",
    FEEDBACK_SUBMISSION_FAILED: "Feedback could not be submitted, please contact Procom team",

    // Email templates
    EMAIL_TEAM_REGISTER_SUBJECT: "PROCOM'19 Team Registration",
    EMAIL_TEAM_REGISTER_BODY_TEXT: "Thanks for being a part of PROCOM19.\nYour registration in competition \"{competition_name}\" has been recorded, you can confirm your registration by paying through one of the following payment methods:\n\n1. Payment to ambassador.\n2. Payment through bank account.\n3. Payment through easy paisa.\n\nYour team code is: {team_code}\nPayment due: {total_fee}\n\nPROCOM MMXIX\n#TriumphingMagnitudes\nWebsite: www.procom19.com\nEmail: procom19help@gmail.com",
    EMAIL_TEAM_REGISTER_BODY_HTML: "Thanks for being a part of PROCOM19.<br>Your registration in competition <b>{competition_name}</b> has been recorded, you can confirm your registration by paying through one of the following payment methods:<br><br>1. Payment to ambassador.<br>2. Payment through bank account.<br>3. Payment through easy paisa.<br><br>Your team code is: {team_code}<br>Payment due: {total_fee}<br><br>PROCOM MMXIX<br>#TriumphingMagnitudes<br>Website: www.procom19.com<br>Email: procom19help@gmail.com",

    EMAIL_TEAM_PAYMENT_SUBJECT: "PROCOM'19 Payment & Registration Confirmation",
    EMAIL_TEAM_PAYMENT_BODY_TEXT: "We have recieved your payment for the \"{competition_name}\" Competition.\nYour registration has been confirmed for PROCOM'19. See you on 20th and 21st of March, 2019!\n\nFollowing are the details of the transaction: \n\nTeam Code: {team_code}\nTeam Name: {team_name}\nCompetition Name: {competition_name}\nPayment Received: {total_fee}\n\nPROCOM MMXIX\n#TriumphingMagnitudes\nWebsite: www.procom19.com\nEmail: procom19help@gmail.com",
    EMAIL_TEAM_PAYMENT_BODY_HTML: "We have recieved your payment for the <b>{competition_name}</b> Competition.<br>Your registration has been confirmed for PROCOM'19. See you on 20th and 21st of March, 2019!<br><br>Following are the details of the transaction: <br><br>Team Code: {team_code}<br>Team Name: {team_name}<br>Competition Name: {competition_name}<br>Payment Received: {total_fee}<br><br>PROCOM MMXIX<br>#TriumphingMagnitudes<br>Website: www.procom19.com<br>Email: procom19help@gmail.com",

    EMAIL_TEAM_PAYMENT_VERIFICATION_SUBJECT: "PROCOM'19 Payment & Registration Confirmation",
    EMAIL_TEAM_PAYMENT_VERIFICATION_BODY_TEXT: "We have verified your payment for the \"{competition_name}\" Competition.\nYour registration has been confirmed for PROCOM'19. See you on 20th and 21st of March, 2019!\n\nFollowing are the details of the transaction: \n\nTeam Code: {team_code}\nTeam Name: {team_name}\nCompetition Name: {competition_name}\nPayment Received: {total_fee}\n\nPROCOM MMXIX\n#TriumphingMagnitudes\nWebsite: www.procom19.com\nEmail: procom19help@gmail.com",
    EMAIL_TEAM_PAYMENT_VERIFICATION_BODY_HTML: "We have verified your payment for the <b>{competition_name}</b> Competition.<br>Your registration has been confirmed for PROCOM'19. See you on 20th and 21st of March, 2019!<br><br>Following are the details of the transaction: <br><br>Team Code: {team_code}<br>Team Name: {team_name}<br>Competition Name: {competition_name}<br>Payment Received: {total_fee}<br><br>PROCOM MMXIX<br>#TriumphingMagnitudes<br>Website: www.procom19.com<br>Email: procom19help@gmail.com",

    EMAIL_CCED_EMAIL_PROCOM_TEAM: "registerprocomnet@gmail.com",

    // Variables here
    ACCOMODATION_CHARGES_PER_HEAD: 1800
}