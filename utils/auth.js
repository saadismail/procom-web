const passport = require('passport');
function authenticate_admin(req, res, next) {
    if(req.user.is_admin) {
        next();
    }
    else res.sendStatus(401);
}

function authenticate_ambassador(req, res, next) {
    if(req.user.is_ambassador) next();
    else res.sendStatus(401);
}

module.exports = {
    authenticate_admin,
    authenticate_ambassador
}