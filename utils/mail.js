const nodemailer = require('nodemailer');
const constants = require('utils/constants');
const config = require('config');

const options = {
    host: config.smtpHost,
    port: config.smtpPort,
    secure: false,
    auth: {
        user: config.smtpUser,
        pass: config.smtpPass
    }
};

const defaults = {
    from: '"PROCOM19" <smtp@procom19.com>', // sender address
    replyTo: "procom19help@gmail.com"
};

async function sendMail(to_email, subject, body_text, body_html) {

    // Do not send mail if not on production server
    if (config.nodeEnv !== "production") {
        return true;
    }

    let transporter = nodemailer.createTransport(options, defaults)

    let mailOptions = {
        to: to_email,
        cc: constants.EMAIL_CCED_EMAIL_PROCOM_TEAM,
        subject: subject,
        text: body_text,
        html: body_html
    }

    try {
        await transporter.sendMail(mailOptions);
    } catch {
        throw constants.EMAIL_COULD_NOT_BE_SENT;
    }
    
    return true;
}

function updateEmail(email_body, competition_name, team_name, team_code, total_fee) {
    return email_body.replace(/{competition_name}/g, competition_name)
        .replace(/{team_code}/g, team_code)
        .replace(/{total_fee}/g, total_fee)
        .replace(/{team_name}/g, team_name);
}

module.exports = {
    sendMail,
    updateEmail
};

