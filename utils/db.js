const config = require('config');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI || config.mongodbString, { useCreateIndex: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../models/user.model'),
    Competition: require("../models/competition.model"),
    Sponsor: require("../models/sponsor.model"),
    Institute: require("../models/institute.model"),
    Team: require("../models/team.model"),
    Payment: require("../models/payment.model"),
    Ambassador: require("../models/ambassador.model"),
    Announcement: require("../models/announcement.model"),
    Promo: require("../models/promo.model"),
    PaymentMethod: require("../models/payment_method.model"),
    Feedback: require("../models/feedback.model")
};