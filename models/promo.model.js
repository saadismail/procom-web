const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    code: { type: String, required: true },
    expiry_date: { type: Date, default: Date.now()+(24*60*60*1000) }
});

module.exports = mongoose.model('Promo', schema);