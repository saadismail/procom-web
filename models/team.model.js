const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const teamMemberSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    phone_no: { type: String },
    cnic: { type: String },
});

const schema = new Schema({
    code: { type: String, unique: true, required: true },
    name: { type: String, required: true },
    competition: { type: Schema.Types.ObjectId, ref: 'Competition', required: true },
    leader: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    is_accomodation: { type: Boolean, required: true, default: false },
    total_fee: { type: Number, required: true },
    promo_code: { type: String },
    members: [teamMemberSchema],
});

module.exports = mongoose.model('Team', schema);