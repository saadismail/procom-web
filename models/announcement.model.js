const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    body: { type: String, required: true },
    sentDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Announcement', schema);