const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: { type: String, required: true },
    level: { type: String, require: true, enum: ['Main', 'Co', 'Platinum', 'Diamond', 'General'] }, // To be specific to values like (Main, Co, Gold, Silver etc)
    photo: { type: String, required: true },
    description: { type: String, required: true },
    website_link: { type: String, required: true }
});

module.exports = mongoose.model('Sponsor', schema);