const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    team: { type: Schema.Types.ObjectId, ref: 'Team', unique: true, required: true },
    total_fee: { type: Number, required: true },
    pay_date: { type: Date, default: Date.now },
    is_verified: { type: Boolean, default: false },

    // Only applicable on ambassador payments
    ambassador: { type: Schema.Types.ObjectId, ref: 'User' },

    // Only applicable on self-payments
    submittor: { type: Schema.Types.ObjectId, ref: 'User' },
    trx_id: { type: Number },
    verifier: { type: Schema.Types.ObjectId, ref: 'User' },
    verify_date: { type: Date }
});

module.exports = mongoose.model('Payment', schema);