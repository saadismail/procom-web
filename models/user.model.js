const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    email: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    name: { type: String, required: true },
    institute: { type: Schema.Types.ObjectId, ref: 'Institute', required: true },
    phone_no: { type: String, required: true},
    cnic: { type: String },
    is_ambassador: { type: Boolean, default: false, required: true },
    is_admin: { type: Boolean, default: false, required: true},
    fcm_token: { type: String },
    createdDate: { type: Date, default: Date.now },
});

module.exports = mongoose.model('User', schema);