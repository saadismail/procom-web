const mongoose = require('mongoose');
var slug = require('mongoose-slug-updater');
mongoose.plugin(slug);
const Schema = mongoose.Schema;

const schema = new Schema({
    name: { type: String, unique: true, required: true },
    slug: { type: String, slug: "name" },
    category: { type: String, required: true, enum: ['CS', 'EE', 'BBA', 'GEN', 'GAM']},
    min_members: { type: Number, required: true, min: 1 },
    max_members: { type: Number, required: true, min: 1 },
    per_member_fee: { type: Number, required: true },
    photo: { type: String,  required: true },
    description: { type: String, required: true },
    rules: { type: [String], required: true },
    judging_criteria: { type: String, required: true }
});

module.exports = mongoose.model('Competition', schema);