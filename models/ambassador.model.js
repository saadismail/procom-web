const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    phone_no: { type: String, required: true},
    institute: { type: Schema.Types.ObjectId, ref: 'Institute', required: true },
    department: { type: String, required: true},
    photo: { type: String, required: true},
    createdDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Ambassador', schema);