export const environment = {
  production: true,
  API_HOST: "https://be-api.procom19.com",
  API_PORT: "443"
};