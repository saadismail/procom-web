export const environment = {
  production: true,
  API_HOST: "https://procom19.herokuapp.com",
  API_PORT: "443"
};