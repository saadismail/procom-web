import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UtilityService } from '../../services/utility.service';

@Component({
  selector: 'control-messages',
  templateUrl: './control-messages.component.html',
  styleUrls: ['./control-messages.component.scss']
})
export class ControlMessagesComponent implements OnInit {

  @Input() control: FormControl;

  constructor(private utilityService: UtilityService) { }

  ngOnInit() {
  }

  get errorMessage() {
    for(let propertyName in this.control.errors) {
      if(this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return this.utilityService.getValidationErrorMessage(propertyName, this.control.errors);
      }
    }

    return null;
  }

}
