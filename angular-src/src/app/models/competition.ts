export class Competition {
    constructor(
        public name: string,
        public slug: string,
        public category: "CS" | "EE" | "BBA" | "GEN" | "GAM",
        public min_members: number,
        public max_members: number,
        public per_member_fee: number,
        public photo: string,
        public description: string,
        public rules: string,
        public judging_criteria: string,
        public _id?: string
    ) {

    }
}
