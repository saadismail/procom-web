export class Payment {
    constructor(
        public team: string,
        public total_fee: number,
        public pay_date: Date,
        public is_verified: boolean,
        public ambassador: string,
        public submittor: string,
        public trx_id: number,
        public verifier: any,
        public verify_date: Date,
        public _id: string
    ) {}
}
