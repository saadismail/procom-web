export class Promo {
    constructor(
        public code: string,
        public _id?: number,
        public expiry_date?: Date,
        public is_expired?: boolean
    ) {}
}
