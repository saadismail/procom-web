export class TeamMember {
    constructor(
        public name: string,
        public email: string,
        public phone_no: string,
        public cnic: string
    ) {}
}