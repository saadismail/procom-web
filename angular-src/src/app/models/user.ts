export class User {
    email: string;
    password: string;
    name: string;
    institute: string;
    phone_no: string;
    cnic: string;
    is_ambassador: boolean = false;
}