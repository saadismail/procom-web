import { TeamMember } from './team-member';
import { Promo } from './promo';

export class Team {
    constructor(
        public name: string,
        public leader: string,
        public competition: string,
        public is_accomodation: boolean,
        public promo_code: String,
        public members: TeamMember[]
    ) {}
}
