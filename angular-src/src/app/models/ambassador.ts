import { Institute } from './institute';

export class Ambassador {
    constructor(
        public name: string,
        public email: string,
        public photo: string,
        public phone_no: string,
        public institute: Institute,
        public department: string,
        public _id?: string
    ) {
    }
}