export class ProcomTeamMember {
    constructor(
        public name: string,
        public designation: string,
        public photo: string) {
        }
        
        // public phone: string,
        // public email: string,
        // public description: string,
        // public fbLink: string,
        // public twitterLink: string,
        
}