export class Sponsor {
    constructor(
        public name: string,
        public photo: string,
        public description: string,
        public website_link: string,
        public level: string,
        public _id?: string,
    ) {}
}