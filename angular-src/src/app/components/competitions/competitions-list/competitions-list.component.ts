import { Component, OnInit, Input } from '@angular/core';
import { Competition } from '../../../models/competition';
import { Router } from '@angular/router';

@Component({
  selector: 'competitions-list',
  templateUrl: './competitions-list.component.html',
  styleUrls: ['./competitions-list.component.scss']
})
export class CompetitionsListComponent implements OnInit {

  @Input() title: string;
  @Input() competitions: Competition[];
  @Input() size: string;

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
