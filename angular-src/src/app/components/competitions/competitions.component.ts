import { Component, OnInit } from '@angular/core';
import { CompetitionsService } from '../../services/competitions.service';
import { Competition } from '../../models/competition';

@Component({
  selector: 'app-competitions',
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.scss']
})
export class CompetitionsComponent implements OnInit {

  competitions: Competition[];
  cs_competitions: Competition[];
  ee_competitions: Competition[];
  bba_competitions: Competition[];
  gen_competitions: Competition[];
  gam_competitions: Competition[];

  constructor(private competitionsService: CompetitionsService) { }

  ngOnInit() {
    this.competitionsService.getCompetitionByCategory("CS").subscribe(cs_competitions => {
      this.cs_competitions = cs_competitions;
    });

    this.competitionsService.getCompetitionByCategory("EE").subscribe(ee_competitions => {
      this.ee_competitions = ee_competitions;
    });

    this.competitionsService.getCompetitionByCategory("BBA").subscribe(bba_competitions => {
      this.bba_competitions = bba_competitions;
    });

    this.competitionsService.getCompetitionByCategory("GEN").subscribe(gen_competitions => {
      this.gen_competitions = gen_competitions;
    });

    this.competitionsService.getCompetitionByCategory("GAM").subscribe(gam_competitions => {
      this.gam_competitions = gam_competitions;
    });    
  }

}
