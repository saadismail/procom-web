import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Institute } from '../../models/institute';
import { InstitutesService } from '../../services/institutes.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { UtilityService } from '../../services/utility.service';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { MSG_REGISTER_SUCCESSFULLY } from '../../utils/constants';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  institutes$: Observable<Institute[]>;
  selectedInstituteId: Institute;

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private authService: AuthService,
    private utilityService: UtilityService,
    private router: Router,
    private institutesService: InstitutesService) { }

  ngOnInit() {
    this.institutes$ = this.institutesService.getInstitutes();

    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, this.utilityService.emailValidator]],
      password: ['', [Validators.required, this.utilityService.passwordValidator]],
      confirmPassword: ['', [Validators.required, this.utilityService.passwordValidator]],
      institute: [null, Validators.required],
      phoneNo: ['', [Validators.required, this.utilityService.phoneNoValidator]],
      cnic: ['', this.utilityService.cnicValidator],
      recaptcha: new FormControl(null, Validators.required)
    }, {
      validators: this.utilityService.passwordMatchValidator
    });
  }

  onRegisterSubmit(){

    if(this.registerForm.dirty && this.registerForm.valid) {
      const user: User = {
        name: this.registerForm.controls.name.value,
        email: this.registerForm.controls.email.value,
        password: this.registerForm.controls.password.value,
        institute: this.registerForm.controls.institute.value._id,
        phone_no: this.registerForm.controls.phoneNo.value,
        cnic: this.registerForm.controls.cnic.value,
        is_ambassador: false
      }
      this.usersService.register(user).subscribe(
        data => {
          if (data.success) {
            // Login user after registeration
            this.usersService.authenticate(user.email, user.password).subscribe(
              data => {
                if (data.success) {
                  this.utilityService.showFlashSuccess(MSG_REGISTER_SUCCESSFULLY);
                  this.authService.login(data.user);
                  this.router.navigate(['/']);
                } else {
                  this.utilityService.showFlashError(data.message);
                  this.registerForm.controls.recaptcha.reset();
                }
              },
              err => {
                this.utilityService.showFlashError(this.utilityService.handleError(err));
                this.registerForm.controls.recaptcha.reset();
              }
            );
          } else {
            this.utilityService.showFlashError(data.message);
            this.registerForm.controls.recaptcha.reset();
          }
        },
        err => {
          this.utilityService.showFlashError(this.utilityService.handleError(err));
          this.registerForm.controls.recaptcha.reset();
        }
      );
    }
  }

}
