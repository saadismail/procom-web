import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Competition } from '../../models/competition';
import { CompetitionsService } from '../../services/competitions.service';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss']
})
export class CompetitionComponent implements OnInit {

  competition: Competition;

  constructor(private route: ActivatedRoute, private competitionsService: CompetitionsService) { }

  ngOnInit() {
    let slug = this.route.snapshot.paramMap.get('id');
    
    this.competitionsService.getCompetition(slug).subscribe(competition => {
      this.competition = competition;
    });
  }

}
