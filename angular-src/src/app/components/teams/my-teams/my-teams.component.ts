import { Component, OnInit } from '@angular/core';
import { TeamsService } from '../../../services/teams.service';
import { Team } from '../../../models/team';

@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss']
})
export class MyTeamsComponent implements OnInit {

  teams: Team[];

  constructor(
    private teamsService: TeamsService
  ) { }

  ngOnInit() {
    this.teamsService.getMyTeams().subscribe(teams => {
      this.teams = teams;
    })
  }

}
