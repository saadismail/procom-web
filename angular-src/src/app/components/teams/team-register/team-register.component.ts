import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UtilityService } from '../../../services/utility.service';
import { Observable } from 'rxjs';
import { CompetitionsService } from '../../../services/competitions.service';
import { Competition } from '../../../models/competition';
import { AuthService } from '../../../services/auth.service';
import { TeamMember } from '../../../models/team-member';
import { TeamsService } from '../../../services/teams.service';
import { Team } from '../../../models/team';
import { Router } from '@angular/router';
import { MSG_TEAM_REGISTERED_SUCCESSFULLY, MSG_TEAM_REGISTRATION_FAILED, MSG_TEAM_LENGTH_SHORT, MSG_TEAM_LENGTH_LONG } from '../../../utils/constants';

@Component({
  selector: 'app-team-register',
  templateUrl: './team-register.component.html',
  styleUrls: ['./team-register.component.scss']
})
export class TeamRegisterComponent implements OnInit {

  currentUser: any;
  competitions$: Observable<Competition[]>;
  registerTeamForm: FormGroup;
  members: TeamMember[];

  constructor(
    private formBuilder: FormBuilder,
    private utilityService: UtilityService,
    private authService: AuthService,
    private teamsService: TeamsService,
    private competitionsService: CompetitionsService,
    private router: Router  
  )
  { }

  memberFieldsInvalid() {
    return (
      this.registerTeamForm.controls.memberName.invalid ||
      this.registerTeamForm.controls.memberEmail.invalid ||
      this.registerTeamForm.controls.memberPhoneNo.invalid ||
      this.registerTeamForm.controls.memberCnic.invalid
    );
  }

  ngOnInit() {
    this.currentUser = this.authService.currentUser;
    this.members = [];
    this.members.push(new TeamMember(
      this.currentUser.name,
      this.currentUser.email,
      this.currentUser.phone_no,
      this.currentUser.cnic
    ));

    this.competitions$ = this.competitionsService.getCompetitions();

    this.registerTeamForm = this.formBuilder.group({
      name: ['', Validators.required],
      competition: [null, Validators.required],
      promoCode: [''],
      isAccomodation: [false, Validators.required],
      memberName: ['', Validators.required],
      memberEmail: ['', [Validators.required, this.utilityService.emailValidator]],
      memberPhoneNo: ['', this.utilityService.phoneNoValidator],
      memberCnic: ['', this.utilityService.cnicValidator],
      recaptcha: new FormControl(null, Validators.required)
    });
  }

  addTeamMember() {
    event.preventDefault();
  
    if(!this.memberFieldsInvalid()) {
      this.members.push(new TeamMember(
        this.registerTeamForm.controls.memberName.value,
        this.registerTeamForm.controls.memberEmail.value,
        this.registerTeamForm.controls.memberPhoneNo.value,
        this.registerTeamForm.controls.memberCnic.value
      ));

      this.registerTeamForm.controls.memberName.setValue('');
      this.registerTeamForm.controls.memberEmail.setValue('');
      this.registerTeamForm.controls.memberPhoneNo.setValue('');
      this.registerTeamForm.controls.memberCnic.setValue('');
    }
  }

  removeTeamMember(index) {
    event.preventDefault();
    this.members.splice(index, 1);
  }

  onRegisterTeamSubmit() {
    this.competitionsService.getCompetition(this.registerTeamForm.controls.competition.value.slug).subscribe(competition => {
      if(
        (
          this.registerTeamForm.dirty &&
          this.registerTeamForm.valid
        ) ||
        (
          this.memberFieldsInvalid() &&
          this.members.length >= competition.min_members
        )
      ) {
        if(this.members.length < competition.min_members) {
          this.utilityService.showFlashError(MSG_TEAM_LENGTH_SHORT);
        } else if(this.members.length > competition.max_members) {
          this.utilityService.showFlashError(MSG_TEAM_LENGTH_LONG);
        } else {
          const team: Team = new Team(
            this.registerTeamForm.controls.name.value,
            this.currentUser._id,
            this.registerTeamForm.controls.competition.value,
            this.registerTeamForm.controls.isAccomodation.value, 
            this.registerTeamForm.controls.promoCode.value,
            this.members
          );
    
          this.teamsService.register(team).subscribe(data => {
            if(data.success) {
              this.utilityService.showFlashSuccess(MSG_TEAM_REGISTERED_SUCCESSFULLY);
              this.router.navigate(['/teams']);
            } else {
              this.utilityService.showFlashError(data.message);
              this.registerTeamForm.controls.recaptcha.reset();
            }
          }, err => {
            this.utilityService.showFlashError(this.utilityService.handleError(err));
            this.registerTeamForm.controls.recaptcha.reset();
          });

        }
      } else {
        if(this.members.length < competition.min_members) {
          this.utilityService.showFlashError(MSG_TEAM_LENGTH_SHORT);
        }
      }
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
      this.registerTeamForm.controls.recaptcha.reset();
    });
  }

}