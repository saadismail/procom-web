import { Component, OnInit } from '@angular/core';
import { Ambassador } from '../../models/ambassador';
import { AmbassadorsService } from '../../services/ambassadors.service';

@Component({
  selector: 'app-ambassadors',
  templateUrl: './ambassadors.component.html',
  styleUrls: ['./ambassadors.component.scss']
})
export class AmbassadorsComponent implements OnInit {

  institutes: any[];

  constructor(private ambassadorsService: AmbassadorsService) { }

  ngOnInit() {
    this.ambassadorsService.getAmbassadors().subscribe(institutes => {
      this.institutes = institutes;
    });
  }

}
