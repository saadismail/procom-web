import { Component, OnInit } from '@angular/core';
import { SponsorsService } from '../../services/sponsors.service';
import { Sponsor } from '../../models/sponsor';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent implements OnInit {

  mainSponsors: Sponsor[];
  coSponsors: Sponsor[];
  diamondSponsors: Sponsor[];
  platinumSponsors: Sponsor[];
  otherSponsors: Sponsor[];

  constructor(private sponsorsService: SponsorsService) { }

  ngOnInit() {
    this.sponsorsService.getSponsorByCategory("Main").subscribe(sponsors => {
      this.mainSponsors = sponsors;
    });

    this.sponsorsService.getSponsorByCategory("Co").subscribe(sponsors => {
      this.coSponsors = sponsors;
    });

    this.sponsorsService.getSponsorByCategory("Diamond").subscribe(sponsors => {
      this.diamondSponsors = sponsors;
    })

    this.sponsorsService.getSponsorByCategory("General").subscribe(sponsors => {
      this.otherSponsors = sponsors;
    })

    this.sponsorsService.getSponsorByCategory("Platinum").subscribe(sponsors => {
      this.platinumSponsors = sponsors;
    })
  }

}
