import { Component, OnInit, Input } from '@angular/core';
import { Sponsor } from '../../../models/sponsor';
@Component({
  selector: 'sponsors-list',
  templateUrl: './sponsors-list.component.html',
  styleUrls: ['./sponsors-list.component.scss']
})
export class SponsorsListComponent implements OnInit {

  @Input() title: string;
  @Input() sponsors: Sponsor[];
  @Input() size: string;

  constructor() { }

  ngOnInit() {
  }

}
