import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { UtilityService } from '../../services/utility.service';
import { Router } from '@angular/router';
import { MSG_LOG_IN_SUCCESSFULLY } from '../../utils/constants';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private authService: AuthService,
    private utilityService: UtilityService,
    private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, this.utilityService.emailValidator]],
      password: ['', [Validators.required, this.utilityService.passwordValidator]],
      recaptcha: new FormControl(null, Validators.required)
    });
  }

  onLoginSubmit() {
    
    if(this.loginForm.dirty && this.loginForm.valid) {
      this.usersService.authenticate(this.loginForm.controls.email.value, this.loginForm.controls.password.value).subscribe(
        data => {
          if (data.success) {
            this.utilityService.showFlashSuccess(MSG_LOG_IN_SUCCESSFULLY);
            this.authService.login(data.user);
            this.router.navigate(['/']);
          } else {
            this.utilityService.showFlashError(data.message);
            this.loginForm.controls.recaptcha.reset();
          }
        },
        err => {
          this.utilityService.showFlashError(this.utilityService.handleError(err));
          this.loginForm.controls.recaptcha.reset();
        }
      );
    }
  }

}
