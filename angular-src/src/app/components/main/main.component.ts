import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
declare var $:any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  constructor(private authService: AuthService) { }

  ngOnInit() {

    this.isLoggedIn$ = this.authService.isLoggedIn;

    var window_width 	 = $(window).width(),
    window_height 		 = window.innerHeight,
    header_height 		 = $(".default-header").height(),
    header_height_static = $(".site-header.static").outerHeight(),
    fitscreen 			 = window_height - header_height;


    $(".fullscreen").css("height", window_height)
    $(".fitscreen").css("height", fitscreen);
    
    $('.active-video-carusel').owlCarousel({
        items:1,
        loop:true,
        dots: true, 
        margin:30,
    });

    $('.active-speaker-carusel').owlCarousel({
        items:2,
        loop:true,
        dots: true,  
        autoplay:true,
        responsive:{
            0:{
                items:1
            },
            1280:{
                items:2
            }
        }
    });
  }

}
