import { Component, OnInit } from '@angular/core';
import { ProcomTeamMember } from '../../../models/procom-team-member';

@Component({
  selector: 'main-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  members: any;

  constructor() { }

  ngOnInit() {
    this.members = {
      "faraz": new ProcomTeamMember(
        "Faraz Ahmed Bayunus",
        "President",
        "assets/img/team/faraz.JPG"
      ),
      "anum": new ProcomTeamMember(
        "Anum Khan",
        "Vice President",
        "assets/img/team/anum.JPG"
      ),
      "humayun": new ProcomTeamMember(
        "Humayun Iqbal",
        "Vice President",
        "assets/img/team/humayun.JPG"
      ),
      "mawiz": new ProcomTeamMember(
        "Mawiz Akrama",
        "Vice President",
        "assets/img/team/mawiz.JPG"
      ),
      "daniyal": new ProcomTeamMember(
        "Daniyal Tahir",
        "General Secretary",
        "assets/img/team/daniyal.JPG"
      ),
      "inseya": new ProcomTeamMember(
        "Inseya Batool",
        "Head Guest Relations",
        "assets/img/team/inseya.JPG"
      ),
      "ammar": new ProcomTeamMember(
        "Ammar Siddiqui",
        "Event Administrator",
        "assets/img/team/ammar.JPG"
      ),
      "bahram": new ProcomTeamMember(
        "Bahram Khan Baloch",
        "President ACM-NUCES",
        "assets/img/team/bahram.JPG"
      ),
      "aruba": new ProcomTeamMember(
        "Aruba Memon",
        "General Secretary",
        "assets/img/team/aruba.jpg"
      ),
      "acm": new ProcomTeamMember(
        "ACM-NUCES KHI Student Chapter",
        "Technical Partner",
        "assets/img/team/acm.JPG"
      )
    }
  }

}