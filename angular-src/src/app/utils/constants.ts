import { environment } from '../../environments/environment';

export const API_BASEURL: string = environment.API_HOST + ":" + environment.API_PORT + "/api";
export const API_ADMIN_BASEURL: string = API_BASEURL + '/admin';

export const API_URL_SPONSORS: string = "backers"; // /sponsors gets blocked by ad blocker hence its /backers
export const API_URL_SPONSORS_CATEGORY: string = "category";

export const API_URL_COMPETITIONS: string = "competitions";
export const API_URL_COMPETITIONS_CATEGORY: string = "category"

export const API_URL_USERS: string = "users";
export const API_URL_USERS_LOGIN: string = "authenticate";
export const API_URL_USERS_REGISTER: string = "register";
export const API_URL_USERS_REGISTER_AS_AMBASSADOR: string = "ambassador";

export const API_URL_INSTITUTES: string = "institutes";

export const API_URL_TEAMS: string = "teams";

export const API_URL_AMBASSADORS: string = "ambassadors";

export const API_URL_PROMOS: string = "promos";

export const API_URL_PAYMENTS: string = "payments";
export const API_URL_PAYMENTS_VERIFY: string = "verify";

export const MSG_SOMETHING_WENT_WRONG = "Something went wrong.";
export const MSG_LOG_IN_SUCCESSFULLY = "Logged in successfully.";
export const MSG_LOG_IN_FAILED = "Log in failed.";
export const MSG_REGISTER_SUCCESSFULLY = "Registered successfully.";

export const MSG_TEAM_REGISTERED_SUCCESSFULLY = "Team registered successfully.";
export const MSG_TEAM_REGISTRATION_FAILED = "Team registration failed.";
export const MSG_TEAM_LENGTH_SHORT = "More members are required for this competition";
export const MSG_TEAM_LENGTH_LONG = "Maximum members length exceeded for this competition";

export const MSG_PROMO_GENERATED_SUCCESSFULLY = "Promo generated successfully.";
export const MSG_PROMO_GENERATION_FAILED = "Promo couldn't be generated.";
export const MSG_PROMO_EXPIRED_SUCCESSFULLY = "Promo expired successfully.";
export const MSG_PROMO_EXPIRATION_FAILED = "Promo couldn't be expired.";
export const MSG_PROMO_REMOVED_SUCCESSFULLY = "Promo removed successfully.";
export const MSG_PROMO_REMOVE_FAILED = "Promo couldn't be removed.";

export const MSG_SPONSOR_CREATED_SUCCESSFULLY = "Sponsor created successfully.";
export const MSG_SPONSOR_REMOVED_SUCCESSFULLY = "Sponsor removed successfully.";
export const MSG_SPONSOR_UPDATED_SUCCESSFULLY = "Sponsor updated successfully.";

export const MSG_AMBASSADOR_CREATED_SUCCESSFULLY = "Ambassador created successfully.";
export const MSG_AMBASSADOR_REMOVED_SUCCESSFULLY = "Ambassador removed successfully.";
export const MSG_AMBASSADOR_UPDATED_SUCCESSFULLY = "Ambassador updated successfully.";

export const MSG_COMPETITION_CREATED_SUCCESSFULLY = "Competition created successfully.";
export const MSG_COMPETITION_REMOVED_SUCCESSFULLY = "Competition removed successfully.";
export const MSG_COMPETITION_UPDATED_SUCCESSFULLY = "Competition updated successfully.";

export const MSG_PAYMENT_VERIFIED_SUCCESSFULLY = "Payment verified successfully.";

export const VALIDATOR_ERR_KEY_PASSWORD_MISMATCH = "passwordMismatch";
export const VALIDATOR_ERR_KEY_INVALID_EMAIL = "invalidEmail";
export const VALIDATOR_ERR_KEY_INVALID_CNIC = "invalidCnic";
export const VALIDATOR_ERR_KEY_INVALID_PHONENO = "invalidPhoneNo";
export const VALIDATOR_ERR_KEY_MIN_LENGTH = "minLength";
export const VALIDATOR_ERR_KEY_MAX_LENGTH = "maxLength";

export const MSG_PASSWORD_MISMATCH = "Password does not match the confirm password.";
export const MSG_MIN_LENGTH = "Minimum length "; //this constant is appended by an integer value when used.
export const MSG_MAX_LENGTH = 'Maximum length '; //this constant is appended by an integer value when used.
export const MSG_REQUIRED = "Required.";
export const MSG_INVALID_EMAIL = "Email is invalid.";
export const MSG_INVALID_PHONENO = "Phone number must be in format 03331234567";
export const MSG_INVALID_CNIC = "CNIC number must be in format 4210112345678";

export const FORM_MODE_ADD = "add";
export const FORM_MODE_EDIT = "edit";