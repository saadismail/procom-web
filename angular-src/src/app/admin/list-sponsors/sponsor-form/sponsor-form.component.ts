import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Sponsor } from '../../../models/sponsor';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SponsorsService } from '../../../services/sponsors.service';
import { UtilityService } from '../../../services/utility.service';
import { MSG_SPONSOR_CREATED_SUCCESSFULLY, FORM_MODE_ADD, FORM_MODE_EDIT, MSG_SPONSOR_UPDATED_SUCCESSFULLY } from '../../../utils/constants';

@Component({
  selector: 'sponsor-form',
  templateUrl: './sponsor-form.component.html',
  styleUrls: ['./sponsor-form.component.scss']
})
export class SponsorFormComponent implements OnInit {

  sponsorForm: FormGroup;

  @Input() sponsor: Sponsor;
  @Input() mode: string;

  @Output() sponsorAdd: EventEmitter<Sponsor> = new EventEmitter<Sponsor>();

  constructor(
    private formBuilder: FormBuilder,
    private sponsorsService: SponsorsService,
    private utilityService: UtilityService
  ) {
    this.sponsorForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      level: ['', [Validators.required]],
      description: ['', [Validators.required]],
      photoUrl: ['', [Validators.required]],
      websiteUrl: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.sponsor) {
      let sponsor = changes.sponsor.currentValue;
      this.sponsorForm.controls.name.setValue(sponsor.name);
      this.sponsorForm.controls.level.setValue(sponsor.level);
      this.sponsorForm.controls.description.setValue(sponsor.description);
      this.sponsorForm.controls.photoUrl.setValue(sponsor.photo);
      this.sponsorForm.controls.websiteUrl.setValue(sponsor.website_link);
    }
  }

  onSponsorSubmit() {
    if(this.sponsorForm.dirty && this.sponsorForm.valid) {
      if(this.mode == FORM_MODE_ADD) {
        this.sponsor = new Sponsor(
          this.sponsorForm.controls.name.value,
          this.sponsorForm.controls.photoUrl.value,
          this.sponsorForm.controls.description.value,
          this.sponsorForm.controls.websiteUrl.value,
          this.sponsorForm.controls.level.value
        );
        this.sponsorsService.createSponsor(this.sponsor).subscribe(data => {
          if(data.success) {
            this.utilityService.showFlashSuccess(MSG_SPONSOR_CREATED_SUCCESSFULLY);
            this.sponsor._id = data.sponsor._id;
            this.sponsorAdd.emit(this.sponsor);
          } else {
            this.utilityService.showFlashError(data.message);
          }
        }, err => this.utilityService.handleError(err));
      } else if(this.mode == FORM_MODE_EDIT) {
        this.sponsorsService.updateSponsor(this.sponsor._id, this.sponsor).subscribe(data => {
          if(data.success) {
            this.utilityService.showFlashSuccess(MSG_SPONSOR_UPDATED_SUCCESSFULLY);
          } else {
            this.utilityService.showFlashError(data.message);
          }
        }, err => this.utilityService.handleError(err));
      }
    }
  }

  resetForm() {
    this.sponsorForm.reset();
    this.mode = FORM_MODE_ADD;
  }

}
