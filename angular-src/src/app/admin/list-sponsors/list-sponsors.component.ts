import { Component, OnInit } from '@angular/core';
import { SponsorsService } from '../../services/sponsors.service';
import { Sponsor } from '../../models/sponsor';
import { UtilityService } from '../../services/utility.service';
import { MSG_SPONSOR_REMOVED_SUCCESSFULLY, FORM_MODE_ADD, FORM_MODE_EDIT } from '../../utils/constants';

@Component({
  selector: 'app-list-sponsors',
  templateUrl: './list-sponsors.component.html',
  styleUrls: ['./list-sponsors.component.scss']
})
export class ListSponsorsComponent implements OnInit {

  addSponsorFormMode: string;
  sponsor: Sponsor;
  sponsors: Sponsor[];

  constructor(
    private sponsorsService: SponsorsService,
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.addSponsorFormMode = FORM_MODE_ADD;
    this.sponsorsService.getSponsors().subscribe(sponsors => {
      this.sponsors = sponsors;
    });
  }

  onSponsorAdd(sponsor) {
    this.sponsors.push(sponsor);
  }

  editSponsor(index) {
    event.preventDefault();
    this.addSponsorFormMode = FORM_MODE_EDIT;
    this.sponsor = this.sponsors[index];
  }

  removeSponsor(index) {
    event.preventDefault();
    this.sponsorsService.removeSponsor(this.sponsors[index]._id).subscribe(data => {
      if(data.success) {
        this.sponsors.splice(index, 1);
        this.utilityService.showFlashSuccess(MSG_SPONSOR_REMOVED_SUCCESSFULLY);
      }
      else this.utilityService.showFlashError(data.message);
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
    });
  }

}
