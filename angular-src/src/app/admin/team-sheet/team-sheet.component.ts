import { Component, OnInit } from '@angular/core';
import { Team } from '../../models/team';
import { TeamsService } from '../../services/teams.service';
import { ExcelService } from '../../services/excel.service';

@Component({
  selector: 'app-team-sheet',
  templateUrl: './team-sheet.component.html',
  styleUrls: ['./team-sheet.component.scss']
})
export class TeamSheetComponent implements OnInit {

  teams: Team[];

  constructor(
    private teamsService: TeamsService,
    private excelService: ExcelService
  ) { }

  ngOnInit() {
    this.teamsService.getAll().subscribe(teams => {
      this.teams = teams;
      this.excelService.exportAsExcelFile(teams, "teams");
    });
  }

}
