import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamSheetComponent } from './team-sheet.component';

describe('TeamSheetComponent', () => {
  let component: TeamSheetComponent;
  let fixture: ComponentFixture<TeamSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
