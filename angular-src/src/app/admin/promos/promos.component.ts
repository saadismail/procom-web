import { Component, OnInit } from '@angular/core';
import { PromosService } from '../../services/promos.service';
import { Promo } from '../../models/promo';
import { UtilityService } from '../../services/utility.service';
import { MSG_PROMO_REMOVED_SUCCESSFULLY, MSG_PROMO_EXPIRED_SUCCESSFULLY, MSG_PROMO_GENERATED_SUCCESSFULLY } from '../../utils/constants';

declare var $: any;

@Component({
  selector: 'app-promos',
  templateUrl: './promos.component.html',
  styleUrls: ['./promos.component.scss']
})
export class PromosComponent implements OnInit {

  code: string;
  promos: Promo[];
  currentDate: number = Date.now();

  constructor(
    private promosService: PromosService,
    private utilityService: UtilityService
  ) {

  }

  ngOnInit() {
    this.promosService.getAll().subscribe(promos => {
      promos.forEach(promo => promo.is_expired = new Date(promo.expiry_date).getTime() < Date.now());
      this.promos = promos;
    });
  }

  generatePromo() {
    this.promosService.generate(new Promo(this.code)).subscribe(data => {
      if(data.success) {
        this.code = "";
        this.promos.push(data.promo);
        this.utilityService.showFlashSuccess(MSG_PROMO_GENERATED_SUCCESSFULLY);
      } else {
        this.utilityService.showFlashError(data.message);
      }
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
    });
  }

  expirePromo(index) {
    event.preventDefault();
    this.promosService.expire(this.promos[index]._id).subscribe(data => {
      if(data.success) {
        this.promos[index].is_expired = true;
        this.promos[index].expiry_date = data.promo.expiry_date;
        this.utilityService.showFlashSuccess(MSG_PROMO_EXPIRED_SUCCESSFULLY);
      }
      else {
        this.utilityService.showFlashError(data.message);
      }
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
    });
    
  }
  
  removePromo(index) {
    event.preventDefault();
    this.promosService.remove(this.promos[index]._id).subscribe(data => {
      if(data.success) {
        this.promos.splice(index, 1);
        this.utilityService.showFlashSuccess(MSG_PROMO_REMOVED_SUCCESSFULLY);
      }
      else this.utilityService.showFlashError(data.message);
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
    });
  }
  

}
