import { Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Ambassador } from '../../../models/ambassador';
import { AmbassadorsService } from '../../../services/ambassadors.service';
import { UtilityService } from '../../../services/utility.service';
import { FORM_MODE_ADD, FORM_MODE_EDIT, MSG_AMBASSADOR_CREATED_SUCCESSFULLY, MSG_AMBASSADOR_UPDATED_SUCCESSFULLY } from '../../../utils/constants';
import { InstitutesService } from '../../../services/institutes.service';
import { Observable } from 'rxjs';
import { Institute } from '../../../models/institute';

@Component({
  selector: 'ambassador-form',
  templateUrl: './ambassador-form.component.html',
  styleUrls: ['./ambassador-form.component.scss']
})
export class AmbassadorFormComponent implements OnInit, OnChanges {

  institutions$: Observable<Institute[]>;
  institutes: Institute[];

  ambassadorForm: FormGroup;

  @Input() ambassador: Ambassador;
  @Input() mode: string;

  @Output() ambassadorAdd: EventEmitter<Ambassador> = new EventEmitter<Ambassador>();
  @Output() ambassadorEdit: EventEmitter<Ambassador> = new EventEmitter<Ambassador>();

  initForm() {
    this.ambassadorForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, this.utilityService.emailValidator]],
      photo: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, this.utilityService.phoneNoValidator]],
      institute: ['', [Validators.required]],
      department: ['', [Validators.required]]
    });
  }

  constructor(
    private formBuilder: FormBuilder,
    private ambassadorsService: AmbassadorsService,
    private utilityService: UtilityService,
    private institutesService: InstitutesService
  ) {
    this.initForm();
  }

  ngOnInit() {
    this.institutions$ = this.institutesService.getInstitutes();
    this.institutions$.subscribe(institutes => {
      this.institutes = institutes;
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.ambassador) {
      let ambassador = changes.ambassador.currentValue;
      this.ambassadorForm.controls.name.setValue(ambassador.name);
      this.ambassadorForm.controls.email.setValue(ambassador.email);
      this.ambassadorForm.controls.photo.setValue(ambassador.photo);
      this.ambassadorForm.controls.phoneNumber.setValue(ambassador.phone_no);
      console.log(ambassador);
      this.ambassadorForm.controls.institute.setValue({_id: ambassador.institute._id, name: ambassador.institute.name});
      this.ambassadorForm.controls.department.setValue(ambassador.department);
    }
  }

  onAmbassadorSubmit() {
    if(this.ambassadorForm.dirty && this.ambassadorForm.valid) {
      if(this.mode == FORM_MODE_ADD) {
        this.ambassador = new Ambassador(
          this.ambassadorForm.controls.name.value,
          this.ambassadorForm.controls.email.value,
          this.ambassadorForm.controls.photo.value,
          this.ambassadorForm.controls.phoneNumber.value,
          this.ambassadorForm.controls.institute.value,
          this.ambassadorForm.controls.department.value,
        );
        this.ambassadorsService.createAmbassador(this.ambassador).subscribe(data => {
          if(data.success) {
            this.utilityService.showFlashSuccess(MSG_AMBASSADOR_CREATED_SUCCESSFULLY);
            this.ambassador._id = data.ambassador._id;
            this.ambassador.institute = data.ambassador.institute.name;
            this.ambassadorAdd.emit(this.ambassador);
          } else {
            this.utilityService.showFlashError(data.message);
          }
        }, err => this.utilityService.handleError(err));
      } else if(this.mode == FORM_MODE_EDIT) {
        // get id of institute for submission of object
        this.ambassador = new Ambassador(
          this.ambassadorForm.controls.name.value,
          this.ambassadorForm.controls.email.value,
          this.ambassadorForm.controls.photo.value,
          this.ambassadorForm.controls.phoneNumber.value,
          this.ambassadorForm.controls.institute.value._id,
          this.ambassadorForm.controls.department.value,
          this.ambassador._id
        );
        this.ambassadorsService.updateAmbassador(this.ambassador._id, this.ambassador).subscribe(data => {
          if(data.success) {
            this.utilityService.showFlashSuccess(MSG_AMBASSADOR_UPDATED_SUCCESSFULLY);
            this.ambassador.institute = this.ambassadorForm.controls.institute.value;
            this.ambassadorEdit.emit(this.ambassador);
            this.resetForm();
          } else {
            this.utilityService.showFlashError(data.message);
          }
        }, err => this.utilityService.handleError(err));
      }
    } else {
      console.log(this.ambassadorForm.errors);
    }
  }

  resetForm() {
    this.initForm();
    this.mode = FORM_MODE_ADD;
  }

}
