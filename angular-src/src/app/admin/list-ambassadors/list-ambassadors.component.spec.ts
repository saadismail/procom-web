import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAmbassadorsComponent } from './list-ambassadors.component';

describe('ListAmbassadorsComponent', () => {
  let component: ListAmbassadorsComponent;
  let fixture: ComponentFixture<ListAmbassadorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAmbassadorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAmbassadorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
