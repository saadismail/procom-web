import { Component, OnInit } from '@angular/core';
import { Ambassador } from '../../models/ambassador';
import { FORM_MODE_ADD, FORM_MODE_EDIT, MSG_AMBASSADOR_REMOVED_SUCCESSFULLY } from '../../utils/constants';
import { AmbassadorsService } from '../../services/ambassadors.service';
import { UtilityService } from '../../services/utility.service';
import { Institute } from '../../models/institute';

@Component({
  selector: 'app-list-ambassadors',
  templateUrl: './list-ambassadors.component.html',
  styleUrls: ['./list-ambassadors.component.scss']
})
export class ListAmbassadorsComponent implements OnInit {

  addAmbassadorFormMode: string;
  ambassador: Ambassador;
  indexOfCurrentEditableAmbassador: number;
  ambassadors: Ambassador[];

  constructor(
    private ambassadorsService: AmbassadorsService,
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.addAmbassadorFormMode = FORM_MODE_ADD;
    this.ambassadorsService.getAmbassadors().subscribe(institutes => {
      institutes.forEach(institute => {
        console.log(institute);
        if(!this.ambassadors) this.ambassadors = [];
        institute.ambassadors.map(ambassador => {
          ambassador.institute = new Institute(institute.name, institute._id);
          return ambassador;
        });
        this.ambassadors.push(...institute.ambassadors);
      });
      
    });
  }

  onAmbassadorAdd(ambassador) {
    this.ambassadors.push(ambassador);
  }

  onAmbassadorEdit(ambassador) {
    this.ambassadors[this.indexOfCurrentEditableAmbassador] = ambassador;
    this.ambassador.institute = ambassador.institute;
  }

  editAmbassador(index) {
    event.preventDefault();
    this.addAmbassadorFormMode = FORM_MODE_EDIT;
    this.indexOfCurrentEditableAmbassador = index;
    this.ambassador = this.ambassadors[index];
  }

  removeAmbassador(index) {
    event.preventDefault();
    this.ambassadorsService.removeAmbassador(this.ambassadors[index]._id).subscribe(data => {
      if(data.success) {
        this.ambassadors.splice(index, 1);
        this.utilityService.showFlashSuccess(MSG_AMBASSADOR_REMOVED_SUCCESSFULLY);
      }
      else this.utilityService.showFlashError(data.message);
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
    });
  }

}
