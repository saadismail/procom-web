import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentVerificationsComponent } from './payment-verifications.component';

describe('PaymentVerificationsComponent', () => {
  let component: PaymentVerificationsComponent;
  let fixture: ComponentFixture<PaymentVerificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentVerificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentVerificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
