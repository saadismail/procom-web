import { Component, OnInit } from '@angular/core';
import { Payment } from '../../models/payment';
import { PaymentsService } from '../../services/payments.service';
import { UtilityService } from '../../services/utility.service';
import { MSG_PAYMENT_VERIFIED_SUCCESSFULLY } from '../../utils/constants';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-payment-verifications',
  templateUrl: './payment-verifications.component.html',
  styleUrls: ['./payment-verifications.component.scss']
})
export class PaymentVerificationsComponent implements OnInit {

  payments: Payment[];

  constructor(
    private paymentsService: PaymentsService,
    private utilityService: UtilityService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.paymentsService.getAll().subscribe(payments => {
      this.payments = payments;
    });
  }

  verifyPayment(index) {
    event.preventDefault();
    this.paymentsService.verifyPayment(this.payments[index]._id).subscribe(data => {
      if(data.success) {
        this.utilityService.showFlashSuccess(MSG_PAYMENT_VERIFIED_SUCCESSFULLY);
        this.payments[index].is_verified = true;
        this.payments[index].verifier = {};
        this.payments[index].verifier.name = this.authService.currentUser.name;
        this.payments[index].verify_date = data.verify_date;
      } else {
        this.utilityService.showFlashError(data.message);
      }
    }, err => this.utilityService.showFlashError(this.utilityService.handleError(err)));
  }

}
