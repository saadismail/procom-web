import { Component, OnInit } from '@angular/core';
import { CompetitionsService } from '../../services/competitions.service';
import { UtilityService } from '../../services/utility.service';
import { FORM_MODE_ADD, FORM_MODE_EDIT, MSG_COMPETITION_REMOVED_SUCCESSFULLY } from '../../utils/constants';
import { Competition } from '../../models/competition';

@Component({
  selector: 'app-list-competitions',
  templateUrl: './list-competitions.component.html',
  styleUrls: ['./list-competitions.component.scss']
})
export class ListCompetitionsComponent implements OnInit {

  addCompetitionFormMode: string;
  competition: Competition;
  competitions: Competition[];
  indexOfCurrentEditableCompetition: number;
  
  constructor(
    private competitionsService: CompetitionsService,
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.addCompetitionFormMode = FORM_MODE_ADD;
    this.competitionsService.getCompetitions().subscribe(Competitions => {
      this.competitions = Competitions;
    });
  }

  onCompetitionAdd(Competition) {
    this.competitions.push(Competition);
  }

  onCompetitionEdit(competition) {
    this.competitions[this.indexOfCurrentEditableCompetition] = competition;
  }

  editCompetition(index) {
    event.preventDefault();
    this.addCompetitionFormMode = FORM_MODE_EDIT;
    this.competition = this.competitions[index];
  }

  removeCompetition(index) {
    event.preventDefault();
    this.competitionsService.removeCompetition(this.competitions[index]._id).subscribe(data => {
      if(data.success) {
        this.competitions.splice(index, 1);
        this.utilityService.showFlashSuccess(MSG_COMPETITION_REMOVED_SUCCESSFULLY);
      }
      else this.utilityService.showFlashError(data.message);
    }, err => {
      this.utilityService.showFlashError(this.utilityService.handleError(err));
    });
  }

}
