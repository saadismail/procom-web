import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCompetitionsComponent } from './list-competitions.component';

describe('ListCompetitionsComponent', () => {
  let component: ListCompetitionsComponent;
  let fixture: ComponentFixture<ListCompetitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCompetitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCompetitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
