import { Component, OnInit, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Competition } from '../../../models/competition';
import { CompetitionsService } from '../../../services/competitions.service';
import { UtilityService } from '../../../services/utility.service';
import { FORM_MODE_ADD, MSG_COMPETITION_CREATED_SUCCESSFULLY, FORM_MODE_EDIT, MSG_COMPETITION_UPDATED_SUCCESSFULLY } from '../../../utils/constants';

@Component({
  selector: 'competition-form',
  templateUrl: './competition-form.component.html',
  styleUrls: ['./competition-form.component.scss']
})
export class CompetitionFormComponent implements OnInit {

  isEdittingRule: boolean;
  indexOfCurrentEditableRule: number;

  competitionForm: FormGroup;

  @Input() competition: Competition;
  @Input() mode: string;

  @Output() competitionAdd: EventEmitter<Competition> = new EventEmitter<Competition>();
  @Output() competitionEdit: EventEmitter<Competition> = new EventEmitter<Competition>();

  initForm() {
    this.competitionForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      slug: [''],
      category: ['', [Validators.required]],
      min_members: ['', [Validators.required]],
      max_members: ['', [Validators.required]],
      per_member_fee: ['', [Validators.required]],
      photo: ['', [Validators.required]],
      description: ['', [Validators.required]],
      judging_criteria: ['', [Validators.required]],
      rules: [[]],
      rule: [[]]
    });
  }

  constructor(
    private formBuilder: FormBuilder,
    private competitionsService: CompetitionsService,
    private utilityService: UtilityService
  ) {
    this.initForm();
  }

  ngOnInit() {
    
  }

  moveUpRule(index) {
    event.preventDefault();
    let tmpRule = this.competitionForm.controls.rules.value[index];
    if(index == 0) {
      this.competitionForm.controls.rules.value[index] = this.competitionForm.controls.rules.value[this.competitionForm.controls.rules.value.length-1];
      this.competitionForm.controls.rules.value[this.competitionForm.controls.rules.value.length-1] = tmpRule;
    }
    else {
      this.competitionForm.controls.rules.value[index] = this.competitionForm.controls.rules.value[index-1];
      this.competitionForm.controls.rules.value[index-1] = tmpRule;
    }
  }
  
  moveDownRule(index) {
    event.preventDefault();
    let tmpRule = this.competitionForm.controls.rules.value[index];
    if(index == this.competitionForm.controls.rules.value.length-1) {
      this.competitionForm.controls.rules.value[index] = this.competitionForm.controls.rules.value[0];
      this.competitionForm.controls.rules.value[0] = tmpRule;
    }
    else {
      this.competitionForm.controls.rules.value[index] = this.competitionForm.controls.rules.value[index+1];
      this.competitionForm.controls.rules.value[index+1] = tmpRule;
    }
  }

  editRule(index) {
    event.preventDefault();
    this.indexOfCurrentEditableRule = index;
    this.isEdittingRule = true;
    this.competitionForm.controls.rule.setValue(this.competitionForm.controls.rules.value[index]);
  }

  removeRule(index) {
    event.preventDefault();
    this.competitionForm.controls.rules.value.splice(index, 1);
  }

  addOrEditRule() {
    if(this.isEdittingRule) {
      this.competitionForm.controls.rules.value[this.indexOfCurrentEditableRule] = this.competitionForm.controls.rule.value;
    } else {
      this.competitionForm.controls.rules.value.push(this.competitionForm.controls.rule.value);
    }
    this.isEdittingRule = false;
    this.competitionForm.controls.rule.reset();
    this.competitionForm.controls.rule.clearValidators();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.competition) {
      let competition = changes.competition.currentValue;
      this.competitionForm.controls.name.setValue(competition.name);
      this.competitionForm.controls.slug.setValue(competition.slug);
      this.competitionForm.controls.category.setValue(competition.category);
      this.competitionForm.controls.min_members.setValue(competition.min_members);
      this.competitionForm.controls.max_members.setValue(competition.max_members);
      this.competitionForm.controls.per_member_fee.setValue(competition.per_member_fee);
      this.competitionForm.controls.photo.setValue(competition.photo);
      this.competitionForm.controls.description.setValue(competition.description);
      this.competitionForm.controls.judging_criteria.setValue(competition.judging_criteria);
      this.competitionForm.controls.rules.setValue(competition.rules);
    }
  }

  onCompetitionSubmit() {
    if(this.competitionForm.dirty && this.competitionForm.valid) {
      if(this.mode == FORM_MODE_ADD) {
        this.competition = new Competition(
          this.competitionForm.controls.name.value,
          this.competitionForm.controls.slug.value,
          this.competitionForm.controls.category.value,
          this.competitionForm.controls.min_members.value,
          this.competitionForm.controls.max_members.value,
          this.competitionForm.controls.per_member_fee.value,
          this.competitionForm.controls.photo.value,
          this.competitionForm.controls.description.value,
          this.competitionForm.controls.judging_criteria.value,
          this.competitionForm.controls.rules.value
        );
        this.competitionsService.createCompetition(this.competition).subscribe(data => {
          if(data.success) {
            this.utilityService.showFlashSuccess(MSG_COMPETITION_CREATED_SUCCESSFULLY);
            this.competition._id = data.competition._id;
            this.competitionAdd.emit(this.competition);
          } else {
            this.utilityService.showFlashError(data.message);
          }
        }, err => this.utilityService.handleError(err));
      } else if(this.mode == FORM_MODE_EDIT) {
        // get id of institute for submission of object
        this.competition = new Competition(
          this.competitionForm.controls.name.value,
          this.competitionForm.controls.slug.value,
          this.competitionForm.controls.category.value,
          this.competitionForm.controls.min_members.value,
          this.competitionForm.controls.max_members.value,
          this.competitionForm.controls.per_member_fee.value,
          this.competitionForm.controls.photo.value,
          this.competitionForm.controls.description.value,
          this.competitionForm.controls.judging_criteria.value,
          this.competitionForm.controls.rules.value,
          this.competition._id
        );
        this.competitionsService.updateCompetition(this.competition._id, this.competition).subscribe(data => {
          if(data.success) {
            this.utilityService.showFlashSuccess(MSG_COMPETITION_UPDATED_SUCCESSFULLY);
            this.competitionEdit.emit(this.competition);
            this.resetForm();
          } else {
            this.utilityService.showFlashError(data.message);
          }
        }, err => this.utilityService.handleError(err));
      }
    } else {
      console.log(this.competitionForm);
    }
  }

  resetForm() {
    this.initForm();
    this.mode = FORM_MODE_ADD;
  }

}
