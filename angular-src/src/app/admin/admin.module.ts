import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromosComponent } from './promos/promos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListSponsorsComponent } from './list-sponsors/list-sponsors.component';
import { SharedModule } from '../shared/shared.module';
import { SponsorFormComponent } from './list-sponsors/sponsor-form/sponsor-form.component';
import { ListAmbassadorsComponent } from './list-ambassadors/list-ambassadors.component';
import { AmbassadorFormComponent } from './list-ambassadors/ambassador-form/ambassador-form.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CompetitionFormComponent } from './list-competitions/competition-form/competition-form.component';
import { ListCompetitionsComponent } from './list-competitions/list-competitions.component';
import { PaymentVerificationsComponent } from './payment-verifications/payment-verifications.component';
import { TeamSheetComponent } from './team-sheet/team-sheet.component';

@NgModule({
  declarations: [PromosComponent, ListSponsorsComponent, SponsorFormComponent, ListAmbassadorsComponent, AmbassadorFormComponent, CompetitionFormComponent, ListCompetitionsComponent, PaymentVerificationsComponent, TeamSheetComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgSelectModule
  ]
})
export class AdminModule { }
