import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { AdminModule } from './admin/admin.module';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SponsorsComponent } from './components/sponsors/sponsors.component';
import { MainComponent } from './components/main/main.component';
import { CompetitionsComponent } from './components/competitions/competitions.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SponsorsListComponent } from './components/sponsors/sponsors-list/sponsors-list.component';
import { CompetitionsListComponent } from './components/competitions/competitions-list/competitions-list.component';
import { CompetitionComponent } from './components/competition/competition.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BannerComponent } from './components/main/banner/banner.component';
import { HighlightsComponent } from './components/main/highlights/highlights.component';
import { TeamComponent } from './components/main/team/team.component';
import { FeaturesComponent } from './components/main/features/features.component';
import { TeamRegisterComponent } from './components/teams/team-register/team-register.component';
import { MyTeamsComponent } from './components/teams/my-teams/my-teams.component';
import { AmbassadorsComponent } from './components/ambassadors/ambassadors.component';
import { FooterComponent } from './components/footer/footer.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    SponsorsComponent,
    MainComponent,
    CompetitionsComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    SponsorsListComponent,
    CompetitionsListComponent,
    CompetitionComponent,
    NavbarComponent,
    BannerComponent,
    HighlightsComponent,
    TeamComponent,
    FeaturesComponent,
    TeamRegisterComponent,
    MyTeamsComponent,
    AmbassadorsComponent,
    FooterComponent,
    UnauthorizedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    FlashMessagesModule.forRoot(),
    LoadingBarHttpClientModule,
    SharedModule,
    AdminModule
  ],
  providers: [{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LcW_IwUAAAAAEFX5gXEMuWmI5GapDpe3XHgMWwh'
    } as RecaptchaSettings
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
