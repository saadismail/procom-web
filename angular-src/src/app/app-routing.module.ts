import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SponsorsComponent } from './components/sponsors/sponsors.component';
import { MainComponent } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CompetitionsComponent } from './components/competitions/competitions.component';
import { CompetitionComponent } from './components/competition/competition.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TeamRegisterComponent } from './components/teams/team-register/team-register.component';
import { MyTeamsComponent } from './components/teams/my-teams/my-teams.component';
import { AuthGuard } from './guards/auth.guard';
import { AmbassadorsComponent } from './components/ambassadors/ambassadors.component';
import { PromosComponent } from './admin/promos/promos.component';
import { AdminAuthGuard } from './guards/admin-auth.guard';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { ListSponsorsComponent } from './admin/list-sponsors/list-sponsors.component';
import { ListAmbassadorsComponent } from './admin/list-ambassadors/list-ambassadors.component';
import { ListCompetitionsComponent } from './admin/list-competitions/list-competitions.component';
import { PaymentVerificationsComponent } from './admin/payment-verifications/payment-verifications.component';
import { TeamSheetComponent } from './admin/team-sheet/team-sheet.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: 'sponsors',
    component: SponsorsComponent
  },
  {
    path: 'competitions',
    component: CompetitionsComponent
  },
  {
    path: 'competitions/:id',
    component: CompetitionComponent
  },
  {
    path: 'teams',
    component: MyTeamsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'teams/register',
    component: TeamRegisterComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'ambassadors',
    component: AmbassadorsComponent
  },
  {
    path: 'junoonizback/promos',
    component: PromosComponent,
    canActivate: [AuthGuard, AdminAuthGuard]
  },
  {
    path: 'junoonizback/sponsors',
    component: ListSponsorsComponent,
    canActivate: [AuthGuard, AdminAuthGuard]
  },
  {
    path: 'junoonizback/ambassadors',
    component: ListAmbassadorsComponent,
    canActivate: [AuthGuard, AdminAuthGuard]
  },
  {
    path: 'junoonizback/competitions',
    component: ListCompetitionsComponent,
    canActivate: [AuthGuard, AdminAuthGuard]
  },
  {
    path: 'junoonizback/payment-verifications',
    component: PaymentVerificationsComponent,
    canActivate: [AuthGuard, AdminAuthGuard]
  },
  {
    path: 'junoonizback/team-sheet',
    component: TeamSheetComponent,
    canActivate: [AuthGuard, AdminAuthGuard]
  },
  {
    path: 'unauthorized',
    component: UnauthorizedComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
