import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Promo } from '../models/promo';
import { API_URL_PROMOS, API_ADMIN_BASEURL } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class PromosService {

  headers: HttpHeaders;

  constructor(
    private httpClient: HttpClient
  ) {
      this.headers = new HttpHeaders({
        'Authorization': localStorage.getItem('id_token')
      });
  }

  getAll() {
    return this.httpClient.get<Promo[]>(API_ADMIN_BASEURL + '/' + API_URL_PROMOS, {
      headers: this.headers
    });
  }

  generate(promo: Promo) {
    return this.httpClient.post<any>(API_ADMIN_BASEURL + '/' + API_URL_PROMOS, promo, {
      headers: this.headers
    });
  }

  expire(promoId) {
    return this.httpClient.post<any>(API_ADMIN_BASEURL + '/' + API_URL_PROMOS + '/' + promoId + '/expire', null, {
      headers: this.headers
    });
  }

  remove(promoId) {
    return this.httpClient.delete<any>(API_ADMIN_BASEURL + '/' + API_URL_PROMOS + '/' + promoId, {
      headers: this.headers
    });
  }
}
