import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_ADMIN_BASEURL, API_URL_PAYMENTS, API_URL_PAYMENTS_VERIFY } from '../utils/constants';
import { Payment } from '../models/payment';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  headers: HttpHeaders;

  constructor(
    private httpClient: HttpClient
  ) {
      this.headers = new HttpHeaders({
        'Authorization': localStorage.getItem('id_token')
      });
  }

  getAll() {
    return this.httpClient.get<Payment[]>(API_ADMIN_BASEURL + '/' + API_URL_PAYMENTS, {
      headers: this.headers
    });
  }

  verifyPayment(id) {
    return this.httpClient.post<any>(API_ADMIN_BASEURL + '/' + API_URL_PAYMENTS + '/' + API_URL_PAYMENTS_VERIFY, {id}, {
      headers: this.headers
    });
  }
}
