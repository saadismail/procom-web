import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_BASEURL, API_URL_USERS, API_URL_USERS_LOGIN, API_URL_USERS_REGISTER } from '../utils/constants';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) { }

  authenticate (email: string, password: string){
    const user = { email, password };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.httpClient.post<any>(API_BASEURL + "/" + API_URL_USERS + "/" + API_URL_USERS_LOGIN, user, httpOptions).pipe();
  }

  register(user: User) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.httpClient.post<any>(API_BASEURL + "/" + API_URL_USERS + "/" + API_URL_USERS_REGISTER, user, httpOptions).pipe();

  }
}
