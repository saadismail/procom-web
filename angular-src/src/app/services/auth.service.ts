import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(!this.isTokenExpired());

  constructor() {

  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  login(user) {
    localStorage.setItem('id_token', user.token);
    localStorage.setItem('user', JSON.stringify(user));
    this.loggedIn.next(true);
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('user');
    this.loggedIn.next(false);
  }

  get currentUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  isTokenExpired() {
    const jwtHelperService = new JwtHelperService();
    if(!localStorage.getItem("id_token")) return true;
    return jwtHelperService.isTokenExpired(localStorage.getItem('id_token'));
  }

}
