import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Institute } from '../models/institute';
import { API_BASEURL, API_URL_INSTITUTES } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class InstitutesService {

  constructor(private httpClient: HttpClient) { }

  getInstitutes() {
    return this.httpClient.get<Institute[]>(API_BASEURL + "/" + API_URL_INSTITUTES);
  }

}
