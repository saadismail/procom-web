import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Competition } from '../models/competition';
import { API_BASEURL, API_URL_COMPETITIONS, API_URL_COMPETITIONS_CATEGORY, API_ADMIN_BASEURL } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class CompetitionsService {

  headers: HttpHeaders;

  constructor(
    private httpClient: HttpClient
  ) {
      this.headers = new HttpHeaders({
        'Authorization': localStorage.getItem('id_token')
      });
  }

  getCompetitions() {
    return this.httpClient.get<Competition[]>(API_BASEURL + "/" + API_URL_COMPETITIONS);
  }

  // VALID CATEGORIES: ["CS" | "EE" | "BBA" | "GEN" | "GAM"]
  getCompetitionByCategory(category) {
    return this.httpClient.get<Competition[]>(API_BASEURL + "/" + API_URL_COMPETITIONS + "/" + API_URL_COMPETITIONS_CATEGORY + "/" + category);
  }

  getCompetition(slug) {
    return this.httpClient.get<Competition>(API_BASEURL + "/" + API_URL_COMPETITIONS + "/" + slug);
  }
  
  createCompetition(competition: Competition) {
    return this.httpClient.post<any>(API_ADMIN_BASEURL + '/' + API_URL_COMPETITIONS, competition, {
      headers: this.headers
    });
  }

  updateCompetition(competitionId, competition: Competition) {
    return this.httpClient.put<any>(API_ADMIN_BASEURL + '/' + API_URL_COMPETITIONS + '/' + competitionId, competition, {
      headers: this.headers
    });
  }

  removeCompetition(competitionId) {
    return this.httpClient.delete<any>(API_ADMIN_BASEURL + '/' + API_URL_COMPETITIONS + '/' + competitionId, {
      headers: this.headers
    });
  }

}
