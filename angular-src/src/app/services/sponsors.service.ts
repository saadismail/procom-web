import { Injectable } from '@angular/core';
import { Sponsor } from '../models/sponsor';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_BASEURL, API_URL_SPONSORS, API_ADMIN_BASEURL, API_URL_SPONSORS_CATEGORY } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class SponsorsService {
  
  headers: HttpHeaders;

  constructor(
    private httpClient: HttpClient
  ) {
      this.headers = new HttpHeaders({
        'Authorization': localStorage.getItem('id_token')
      });
  }

  getSponsors() {
    return this.httpClient.get<Sponsor[]>(API_BASEURL + "/" + API_URL_SPONSORS);
  }

  getSponsorByCategory(category: String) {
    return this.httpClient.get<Sponsor[]>(API_BASEURL + "/" + API_URL_SPONSORS + "/" + API_URL_SPONSORS_CATEGORY + "/" + category);
  }

  createSponsor(sponsor: Sponsor) {
    return this.httpClient.post<any>(API_ADMIN_BASEURL + '/' + API_URL_SPONSORS, sponsor, {
      headers: this.headers
    });
  }

  updateSponsor(sponsorId, sponsor: Sponsor) {
    return this.httpClient.put<any>(API_ADMIN_BASEURL + '/' + API_URL_SPONSORS + '/' + sponsorId, sponsor, {
      headers: this.headers
    });
  }

  removeSponsor(sponsorId) {
    return this.httpClient.delete<any>(API_ADMIN_BASEURL + '/' + API_URL_SPONSORS + '/' + sponsorId, {
      headers: this.headers
    });
  }
}