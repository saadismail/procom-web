import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Team } from '../models/team';
import { API_URL_TEAMS, API_BASEURL, API_ADMIN_BASEURL } from '../utils/constants';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  headers: HttpHeaders;

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {
    this.headers = new HttpHeaders({
      'Authorization': localStorage.getItem('id_token')
    });
  }

  getMyTeams() {
    return this.httpClient.get<Team[]>(API_BASEURL + "/" + API_URL_TEAMS, {
      headers: this.headers
    });
  }

  register(team: Team) {
    return this.httpClient.post<any>(API_BASEURL + "/" + API_URL_TEAMS, team, {
      headers: this.headers
    });
  }

  getAll() {
    return this.httpClient.get<Team[]>(API_ADMIN_BASEURL + "/" + API_URL_TEAMS, {
      headers: this.headers
    });
  }
}
