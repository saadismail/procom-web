import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ambassador } from '../models/ambassador';
import { API_URL_AMBASSADORS, API_BASEURL, API_ADMIN_BASEURL } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class AmbassadorsService {

  headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': localStorage.getItem('id_token')
    });
  }

  getAmbassadors() {
    return this.httpClient.get<any>(API_BASEURL + "/" + API_URL_AMBASSADORS);
  }

  createAmbassador(ambassador: Ambassador) {
    return this.httpClient.post<any>(API_ADMIN_BASEURL + '/' + API_URL_AMBASSADORS, ambassador, {
      headers: this.headers
    });
  }

  updateAmbassador(ambassadorId, ambassador: Ambassador) {
    return this.httpClient.put<any>(API_ADMIN_BASEURL + '/' + API_URL_AMBASSADORS + '/' + ambassadorId, ambassador, {
      headers: this.headers
    });
  }

  removeAmbassador(ambassadorId) {
    return this.httpClient.delete<any>(API_ADMIN_BASEURL + '/' + API_URL_AMBASSADORS + '/' + ambassadorId, {
      headers: this.headers
    });
  }
}
