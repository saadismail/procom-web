import { TestBed } from '@angular/core/testing';

import { AmbassadorsService } from './ambassadors.service';

describe('AmbassadorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AmbassadorsService = TestBed.get(AmbassadorsService);
    expect(service).toBeTruthy();
  });
});
