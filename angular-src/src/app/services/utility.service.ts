import { Injectable } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { MSG_SOMETHING_WENT_WRONG, MSG_PASSWORD_MISMATCH, MSG_MIN_LENGTH, MSG_MAX_LENGTH, MSG_REQUIRED, MSG_INVALID_CNIC, MSG_INVALID_EMAIL, MSG_INVALID_PHONENO, VALIDATOR_ERR_KEY_INVALID_CNIC, VALIDATOR_ERR_KEY_INVALID_EMAIL, VALIDATOR_ERR_KEY_PASSWORD_MISMATCH } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(
    private flashMessagesService: FlashMessagesService
  ) { }

  handleError(error) {
    if (error.error.message) {
      return error.error.message;
    }

    // TO BE IMPLEMENTED (put error messages specific to HTTP status codes).
    return MSG_SOMETHING_WENT_WRONG;
  }
  
  getValidationErrorMessage(validatorKey: string, validatorValue?: any) {
    let config = {
      VALIDATOR_ERR_KEY_PASSWORD_MISMATCH: MSG_PASSWORD_MISMATCH,
      VALIDATOR_ERR_KEY_MIN_LENGTH: MSG_MIN_LENGTH + `${validatorValue.requiredLength}`,
      VALIDATOR_ERR_KEY_MAX_LENGTH: MSG_MAX_LENGTH + `${validatorValue.requiredLength}`,
      'required': MSG_REQUIRED,
      VALIDATOR_ERR_KEY_INVALID_EMAIL: MSG_INVALID_EMAIL,
      VALIDATOR_ERR_KEY_INVALID_PHONENO: MSG_INVALID_PHONENO,
      VALIDATOR_ERR_KEY_INVALID_CNIC: MSG_INVALID_CNIC
    }

    return config[validatorKey];
  }

  emailValidator(control) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(control.value.match(re)) return null;
    else return {VALIDATOR_ERR_KEY_INVALID_EMAIL: true};
  }

  passwordValidator(control) {
    if(control.value.length > 128) return { VALIDATOR_ERR_KEY_MAX_LENGTH: true, requiredLength: '128' };
    if(control.value.length < 8) return { VALIDATOR_ERR_KEY_MIN_LENGTH: true, requiredLength: '8' };
    return null;
  }

  passwordMatchValidator(group) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {VALIDATOR_ERR_KEY_PASSWORD_MISMATCH : true };
  }

  cnicValidator(control) {
    if(!control.value) return null;
    if(control.value.match(/^[0-9]{13}$/)) return null;
    else return { VALIDATOR_ERR_KEY_INVALID_CNIC: true };
  }

  phoneNoValidator(control) {
    if(control.value.match(/^[0-9]{11}$/)) return null;
    else return { VALIDATOR_ERR_KEY_INVALID_PHONENO: true };
  }

  showFlashError(message: string) {
    this.flashMessagesService.show(message, { cssClass: "alert alert-danger", timeout: 3000 });
  }

  showFlashSuccess(message: string) {
    this.flashMessagesService.show(message, { cssClass: "alert alert-success", timeout: 3000 });
  }
}
