if (process.env.NODE_ENV !== "production") {
    const dotenv = require('dotenv');
    dotenv.config();
}

module.exports = {
    nodeEnv: process.env.NODE_ENV,
    nodePort: process.env.PORT,

    smtpHost: process.env.SMTP_HOST,
    smptPort: process.env.SMTP_PORT,
    smtpUser: process.env.SMTP_USER,
    smtpPass: process.env.SMTP_PASS,
    
    mongodbString: process.env.MONGODB_STRING,
    passportSecret: process.env.PASSPORT_SECRET
}